/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.http;

import static org.amdatu.security.tokenprovider.http.TokenUtil.*;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.TokenProviderException;
import org.osgi.service.log.LogService;

/**
 * Provides a filter that can be configured to look for requests that contain either an
 * Authorization header with a specific scheme, or a cookie with a specific name.
 * <p>
 * This filter should be registered as "plain" filter, and can be configured by means of its
 * service properties. Two additional service properties can be used to provide the name of
 * the authorization scheme (<tt>init.authSchemeName</tt>) and the name of the cookie to look
 * for (<tt>init.cookieName</tt>). By default, these two properties are set to "Amdatu" and
 * "amdatu_token".
 * </p>
 * <p>
 * If a token is present, it will be validated against a token provider (which is to be
 * provided by means of a service tracker or by means of DI, like Felix DM) and if valid, the
 * call is pushed further down the filter chain. If an invalid (expired) token is provided,
 * the request is aborted with a "403 Forbidden" response.
 * </p>
 */
public class TokenFilter implements Filter {
    // Locally managed...
    private String m_authSchemeName;
    private String m_cookieName;

    private volatile TokenProvider m_tokenProvider;
    private volatile LogService m_log;

    /**
     * Creates a new {@link TokenFilter} instance.
     */
    public TokenFilter() {
        m_authSchemeName = null;
        m_cookieName = null;
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        String authSchemeName = config.getInitParameter("authSchemeName");
        if (authSchemeName != null && !"".equals(authSchemeName)) {
            m_authSchemeName = authSchemeName;
        }

        String cookieName = config.getInitParameter("cookieName");
        if (cookieName != null && !"".equals(cookieName)) {
            m_cookieName = cookieName;
        }

        if (m_cookieName == null && m_authSchemeName == null) {
            throw new ServletException("Need at least an authentication scheme name or a cookie name!");
        }
    }

    @Override
    public void destroy() {
        // Nop
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String token = null;
        Map<String, String> properties;
        if (m_authSchemeName != null) {
            token = getTokenFromHeader(req, m_authSchemeName);
        }
        if (token == null && m_cookieName != null) {
            token = getTokenFromCookie(req, m_cookieName);
        }

        try {
            properties = m_tokenProvider.verifyToken(token);
        }
        catch (IllegalArgumentException | TokenProviderException e) {
            warn("Failed to verify token: %s from %s:%d!", e, token, req.getRemoteAddr(), req.getRemotePort());

            resp.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        debug("Validated token %s from %s:%d as valid.", token, req.getRemoteAddr(), req.getRemotePort());

        try {
            // Store token in request for the remainder of the chain...
            req.setAttribute(AMDATU_TOKEN_ATTRIBUTE, token);
            req.setAttribute(AMDATU_TOKEN_PROPERTIES_ATTRIBUTE, properties);

            // Token is valid, continue in the filter chain...
            chain.doFilter(req, resp);
        }
        finally {
            // Avoid the token from being stored longer than necessary...
            req.removeAttribute(AMDATU_TOKEN_ATTRIBUTE);
            req.removeAttribute(AMDATU_TOKEN_PROPERTIES_ATTRIBUTE);
        }
    }

    public final void setLogService(LogService log) {
        m_log = log;
    }

    public final void setTokenProvider(TokenProvider provider) {
        m_tokenProvider = provider;
    }

    protected void debug(String msg, Object... args) {
        if (m_log != null) {
            m_log.log(LogService.LOG_DEBUG, String.format(msg, args));
        }
    }

    protected void warn(String msg, Throwable t, Object... args) {
        if (m_log != null) {
            m_log.log(LogService.LOG_WARNING, String.format(msg, args), t);
        }
    }
}
