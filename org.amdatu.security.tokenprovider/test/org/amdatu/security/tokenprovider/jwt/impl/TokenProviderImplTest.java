/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.jwt.impl;

import static org.amdatu.security.tokenprovider.TokenConstants.SUBJECT;
import static org.amdatu.security.tokenprovider.jwt.impl.ConfigUtils.asDictionary;
import static org.amdatu.security.tokenprovider.jwt.impl.TokenProviderConfig.KEY_ALGORITHM;
import static org.amdatu.security.tokenprovider.jwt.impl.TokenProviderConfig.KEY_AUDIENCE;
import static org.amdatu.security.tokenprovider.jwt.impl.TokenProviderConfig.KEY_ISSUER;
import static org.amdatu.security.tokenprovider.jwt.impl.TokenProviderConfig.KEY_KEY_URI;
import static org.amdatu.security.tokenprovider.jwt.impl.TokenProviderConfig.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TokenProviderImplTest {
    @Rule
    public final ExpectedException m_expected = ExpectedException.none();

    private TokenProviderImpl m_tokenProvider;

    @Before
    public void setUp() {
        m_tokenProvider = new TokenProviderImpl();
        m_tokenProvider.setVerificationKeyResolver((sig, struct) -> m_tokenProvider.getConfig().getVerificationKey());
    }

    @Test
    public void testGeneratePredefinedHmacToken() throws Exception {
        Dictionary<String, Object> config = asDictionary(KEY_ALGORITHM, "HS256",
            KEY_TOKEN_VALIDITY_PERIOD, "10", /* seconds */
            KEY_ALLOWED_CLOCK_SKEW, "1", /* seconds */
            KEY_AUDIENCE, "a",
            KEY_ISSUER, "i",
            KEY_KEY_URI, "data:DeadBeefCafeBabeDeadBeefCafeBabeDeadBeefCafeBabe");

        m_tokenProvider.updated(config);

        // Generate a valid token, not expired yet...
        String token = testGenerateAndVerifyToken(Instant.now());

        m_expected.expect(InvalidTokenException.class);

        // Generate a token that was valid 11 (+ 1 for skew) secs. ago, and thus is expired...
        testGenerateAndVerifyToken(Instant.now().minusSeconds(12L));

        config.put(KEY_TOKEN_VALIDITY_PERIOD, "20");
        config.put(KEY_KEY_URI, "data:DeadBeefCafeBabeDeadBeefCafeBabeDeadBeefCafeBabe");
        m_tokenProvider.updated(config);

        m_tokenProvider.verifyToken(token);
    }

    @Test
    public void testGenerateRandomHmacToken() throws Exception {
        m_tokenProvider.updated(asDictionary(KEY_ALGORITHM, "HS256",
            KEY_TOKEN_VALIDITY_PERIOD, "10",
            KEY_ALLOWED_CLOCK_SKEW, "1", /* seconds */
            KEY_AUDIENCE, "a",
            KEY_ISSUER, "i",
            KEY_KEY_URI, "random:/"));

        // Generate a valid token, not expired yet...
        testGenerateAndVerifyToken(Instant.now());

        m_expected.expect(InvalidTokenException.class);

        // Generate a token that was valid 11 (+ 1 for skew) secs. ago, and thus is expired...
        testGenerateAndVerifyToken(Instant.now().minusSeconds(12L));
    }

    @Test
    public void testGenerateRandomHmacTokenByDefault() throws Exception {
        // Generate a valid token, not expired yet...
        testGenerateAndVerifyToken(Instant.now());

        m_expected.expect(InvalidTokenException.class);

        // Generate a token that was valid 3611 seconds ago, and thus is expired...
        testGenerateAndVerifyToken(Instant.now().minusSeconds(3611L));
    }

    @Test
    public void testVerifyInvalidToken() throws Exception {
        m_expected.expectMessage("Token has an invalid signature!");

        // The token has an invalid signature, and a null-claim. The fact that there's a null-claim
        // is present should not cause problems. This tests AMDATUSEC-48...
        m_tokenProvider
            .verifyToken("eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOm51bGx9Cg.bUdi-i34UUX5vYjSGWitl5MNZttzK_0SEWhlBMMpybM");
    }

    private String testGenerateAndVerifyToken(Instant issuedAt) throws Exception {
        Map<String, String> attrs = new HashMap<>();
        attrs.put(SUBJECT, "username");
        attrs.put("foo", "bar");
        attrs.put("qux", "quu");

        // We should be able to generate a token...
        String token = m_tokenProvider.generateToken(issuedAt, attrs);
        assertNotNull(token);

        // This token should be valid...
        Map<String, String> result = m_tokenProvider.verifyToken(token);

        assertTrue(attrs.entrySet().stream().allMatch(p -> p.getValue().equals(result.get(p.getKey()))));

        return token;
    }
}
