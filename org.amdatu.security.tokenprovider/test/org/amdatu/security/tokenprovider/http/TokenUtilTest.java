/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.http;

import static org.amdatu.security.tokenprovider.http.TokenUtil.AUTHORIZATION_HEADER;
import static org.amdatu.security.tokenprovider.http.TokenUtil.getTokenFromCookie;
import static org.amdatu.security.tokenprovider.http.TokenUtil.getTokenFromHeader;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class TokenUtilTest {
    @Mock
    private HttpServletRequest request;

    @Test
    public void testGetTokenFromHeader() {
        String authSchemeName = "authScheme";

        String token = "MyNotSoSecretToken";
        String authHeader = String.format("%s %s", authSchemeName, token);

        when(request.getHeader(eq(AUTHORIZATION_HEADER))).thenReturn(authHeader);

        String tokenFromRequest = getTokenFromHeader(request, authSchemeName);
        assertThat(tokenFromRequest, equalTo(token));

        String authHeaderWithoutSpace = String.format("%s%s", authSchemeName, token);
        when(request.getHeader(eq(AUTHORIZATION_HEADER))).thenReturn(authHeaderWithoutSpace);

        tokenFromRequest = getTokenFromHeader(request, authSchemeName);
        assertThat(tokenFromRequest, nullValue());

        when(request.getHeader(eq(AUTHORIZATION_HEADER))).thenReturn("Basic base64thing");

        tokenFromRequest = getTokenFromHeader(request, authSchemeName);
        assertThat(tokenFromRequest, nullValue());
    }

    @Test
    public void testGetTokenFromCookie() {
        // Given
        Cookie session = new Cookie("JSESSIONID", "dontcare");
        Cookie analytics = new Cookie("_ga", "dontcare");

        Cookie normal = new Cookie("access_token", "normal-token");
        Cookie chunk0 = new Cookie("access_token", "chun");
        Cookie chunk1 = new Cookie("access_token_1", "ked");
        Cookie chunk2 = new Cookie("access_token_2", "-");
        Cookie chunk3 = new Cookie("access_token_3", "tok");
        Cookie chunk4 = new Cookie("access_token_4", "en");

        testGetTokenFromCookie(null, "access_token", null);
        testGetTokenFromCookie(new Cookie[] {}, "access_token", null);
        testGetTokenFromCookie(new Cookie[] {null}, "access_token", null);

        testGetTokenFromCookie(new Cookie[] {normal}, "no_access_token", null);
        testGetTokenFromCookie(new Cookie[] {chunk0, chunk1, chunk2, chunk3, chunk4}, "no_access_token", null);

        testGetTokenFromCookie(new Cookie[] {normal}, "access_token", "normal-token");
        testGetTokenFromCookie(new Cookie[] {chunk0, chunk1, chunk2, chunk3, chunk4}, "access_token", "chunked-token");

        testGetTokenFromCookie(new Cookie[] {session, normal}, "access_token", "normal-token");
        testGetTokenFromCookie(new Cookie[] {normal, analytics}, "access_token", "normal-token");
        testGetTokenFromCookie(new Cookie[] {session, normal, analytics}, "access_token", "normal-token");


        testGetTokenFromCookie(new Cookie[] {session, chunk0, chunk1, chunk2, chunk3, chunk4}, "access_token", "chunked-token");
        testGetTokenFromCookie(new Cookie[] {chunk0, chunk1, chunk2, chunk3, chunk4, analytics}, "access_token", "chunked-token");
        testGetTokenFromCookie(new Cookie[] {session, chunk0, chunk1, chunk2, chunk3, chunk4, analytics}, "access_token", "chunked-token");
        testGetTokenFromCookie(new Cookie[] {chunk0, session, chunk1, chunk2, analytics, chunk3, chunk4}, "access_token", "chunked-token");

        testGetTokenFromCookie(new Cookie[] {chunk3, chunk1, chunk4, chunk2, chunk0}, "access_token", "chunked-token");
    }

    private void testGetTokenFromCookie(Cookie[] cookies, String cookieName, String expectedToken) {
        // Given
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getCookies()).thenReturn(cookies);

        // When
        String token = getTokenFromCookie(request, cookieName);

        // Then
        assertEquals(expectedToken, token);
    }
}
