/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin.rest;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toMap;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.seeOther;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.amdatu.security.account.admin.rest.util.HttpUtil.isXmlHttpRequest;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.amdatu.security.account.AccountException;
import org.amdatu.security.account.AccountLockedException;
import org.amdatu.security.account.AccountValidationException;
import org.amdatu.security.account.NoSuchAccountException;
import org.amdatu.web.util.URIBuilder;

/**
 * Provides all kind of utility and convenience methods for {@link AccountAdminResource}.
 */
class AccountAdminHelper {
    public static final String ACCOUNT_ID = "accountId";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String VERIFY_TOKEN = "verifyToken";
    public static final String EMAIL = "email";

    public static final String CODE_NO_SUCH_ACCOUNT = "no_such_account";
    public static final String CODE_ACCOUNT_INVALID = "invalid_account";
    public static final String CODE_BAD_REQUEST = "bad_request";
    public static final String CODE_CONFLICT = "conflict";
    public static final String CODE_LOCKED = "account_locked";
    public static final String MSG = "msg";

    public static final String ACTION_ACCOUNT_VERIFIED = "account_verified";
    public static final String ACTION_CREDENTIALS_UPDATED = "credentials_updated";
    public static final String ACTION_RESET_REQUESTED = "reset_requested";
    public static final String ACTION_SIGNUP_SUCCESS = "signup_success";
    public static final String ACTION_ACCOUNT_REMOVED = "account_removed";

    private static final String OLD = "old";
    private static final String NEW = "new";
    private static final String VIOLATIONS = "violations";

    static Map<String, Object> asMap(Object... entries) {
        Map<String, Object> result = new HashMap<>();
        for (int i = 0; i < entries.length; i += 2) {
            result.put(entries[i].toString(), entries[i + 1]);
        }
        return result;
    }

    static Map<String, String> getCredentials(Map<String, List<String>> parameters) {
        return parameters.entrySet().stream()
            .collect(toMap(Map.Entry::getKey, e -> {
                List<String> value = e.getValue();
                if (value == null) {
                    return "";
                }
                return String.join(",", urlDecode(value));
            }));
    }

    static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    static Response respondFailure(Status status, String errorCode, Object... additionalProps) {
        Map<String, Object> entity = new HashMap<>();
        entity.put(MSG, errorCode);
        for (int i = 0; i < additionalProps.length; i += 2) {
            entity.put(additionalProps[i].toString(), additionalProps[i + 1]);
        }
        return Response.status(status).entity(entity).build();
    }

    static ResponseBuilder respondSuccess(HttpServletRequest req, Optional<URI> destURI, String msg) {
        ResponseBuilder rb;
        if (isXmlHttpRequest(req) || !destURI.isPresent()) {
            // Ajax request or no success page: respond with an empty OK.
            // (this reduced traffic a bit as the client probably isn't
            // interested in the redirected page but more in the result
            // of the operation)...
            rb = ok(msg);
        }
        else {
            URIBuilder builder = new URIBuilder(destURI.get());
            if (msg != null) {
                builder.appendToQuery(MSG, msg);
            }
            rb = seeOther(builder.build());
        }
        return rb;
    }

    static Response runInSafetyContext(Supplier<Response> supplier, Consumer<Exception> exceptionHandler) {
        try {
            return supplier.get();
        }
        catch (AccountLockedException e) {
            exceptionHandler.accept(e);

            return respondFailure(FORBIDDEN, CODE_LOCKED);
        }
        catch (NoSuchAccountException e) {
            exceptionHandler.accept(e);

            return respondFailure(NOT_FOUND, CODE_NO_SUCH_ACCOUNT);
        }
        catch (AccountValidationException e) {
            exceptionHandler.accept(e);

            return respondFailure(BAD_REQUEST, CODE_ACCOUNT_INVALID, VIOLATIONS, e.getViolations());
        }
        catch (AccountException e) {
            exceptionHandler.accept(e);

            return respondFailure(CONFLICT, CODE_CONFLICT);
        }
        catch (Exception e) {
            exceptionHandler.accept(e);

            return respondFailure(BAD_REQUEST, CODE_BAD_REQUEST);
        }
    }

    static <V> Map<String, V> splitOldAndNewCredentials(Map<String, V> newCredentials) {
        Map<String, V> oldCredentials = new HashMap<>();
        Set<String> keys = new HashSet<>(newCredentials.keySet());
        Set<String> blacklisted = new HashSet<>();

        for (String key : keys) {
            if (blacklisted.contains(key) || splitCredential(key, newCredentials, oldCredentials, blacklisted)) {
                continue;
            }

            if (!blacklisted.contains(key)) {
                oldCredentials.put(key, newCredentials.get(key));
            }
        }

        if (oldCredentials.equals(newCredentials)) {
            return null;
        }

        return oldCredentials;
    }

    static String[] urlDecode(List<String> values) {
        try {
            List<String> decoded = new ArrayList<>(values.size());
            for (String value : values) {
                if (value == null || "".equals(value)) {
                    continue;
                }
                decoded.add(URLDecoder.decode(value, "UTF-8"));
            }
            return decoded.toArray(new String[decoded.size()]);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 must be supported by the JRE!");
        }
    }

    private static <V> boolean splitCredential(String key, Map<String, V> newCreds, Map<String, V> oldCreds, Set<String> handled) {
        String key2 = key.startsWith(NEW) ? key.replaceFirst(NEW, OLD) : key.replaceFirst(OLD, NEW);
        // either the key doesn't start with 'new' or 'old', or both 'newFoo' and 'oldFoo' keys should be present...
        if (key.equals(key2) || !newCreds.containsKey(key) || !newCreds.containsKey(key2)) {
            return false;
        }

        String origKey = unmangleKey(key);
        oldCreds.put(origKey, newCreds.remove(key));
        newCreds.put(origKey, newCreds.remove(key2));

        handled.addAll(asList(key, key2));
        return true;
    }

    private static String unmangleKey(String key) {
        if (key.length() < 4) {
            return key;
        }
        if (Character.isUpperCase(key.charAt(3))) {
            return Character.toLowerCase(key.charAt(3)) + key.substring(4);
        }
        return key;
    }
}
