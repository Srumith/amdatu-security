/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin.rest.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Convenience methods for working with {@link HttpServletRequest}s.
 */
public class HttpUtil {

    private static final String X_REQUESTED_WITH = "X-Requested-With";
    private static final String XML_HTTP_REQUEST = "XMLHttpRequest";

    /**
     * Determines whether or not the given request is originating from an AJAX/XmlHttpRequest call.
     *
     * @param req the servlet request, cannot be <code>null</code>.
     * @return <code>true</code> if the given servlet request is an XML HTTP request, <code>false</code> otherwise.
     */
    public static boolean isXmlHttpRequest(HttpServletRequest req) {
        return XML_HTTP_REQUEST.equals(req.getHeader(X_REQUESTED_WITH));
    }

}
