/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin.rest;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.amdatu.security.account.AccountConstants.KEY_ACCOUNT_ACTION;
import static org.amdatu.security.account.AccountConstants.KEY_ACCOUNT_EMAIL;
import static org.amdatu.security.account.AccountConstants.KEY_ACCOUNT_ID;
import static org.amdatu.security.account.AccountConstants.KEY_ACCOUNT_STATE;
import static org.amdatu.security.account.AccountConstants.KEY_NOTIFY_URL;
import static org.amdatu.security.account.AccountConstants.TOPIC_NOTIFY_ACCOUNT_OWNER;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.ACCESS_TOKEN;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.ACCOUNT_ID;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.ACTION_ACCOUNT_REMOVED;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.ACTION_ACCOUNT_VERIFIED;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.ACTION_CREDENTIALS_UPDATED;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.ACTION_RESET_REQUESTED;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.ACTION_SIGNUP_SUCCESS;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.CODE_BAD_REQUEST;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.EMAIL;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.VERIFY_TOKEN;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.asMap;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.getCredentials;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.isEmpty;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.respondFailure;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.runInSafetyContext;
import static org.amdatu.security.account.admin.rest.AccountAdminHelper.splitOldAndNewCredentials;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.net.URI;
import java.util.Dictionary;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.amdatu.security.account.Account;
import org.amdatu.security.account.AccountAdmin;
import org.amdatu.web.util.URIBuilder;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogService;

/**
 * Provides a REST endpoint for accessing the {@link AccountAdmin}.
 */
@Path("/")
public class AccountAdminResource implements ManagedService {

    // Injected by Felix DM...
    private volatile AccountAdmin m_accountAdmin;
    private volatile EventAdmin m_eventAdmin;
    private volatile LogService m_log;
    // Locally managed...
    private volatile AccountAdminResourceConfig m_config;
    // Injected by JAXRS...
    @Context
    private volatile HttpServletRequest m_request;

    @GET
    @Path("/accounts/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response accountExists(@PathParam("id") String accountId) {
        if (isEmpty(accountId)) {
            m_log.log(LOG_INFO, "Failed to process credentials reset request for user: no/invalid accountId!");

            return respondFailure(BAD_REQUEST, CODE_BAD_REQUEST);
        }

        return runInSafetyContext(() -> {
            return Response.ok(m_accountAdmin.accountExists(accountId)).build();
        }, e -> m_log.log(LOG_INFO, "Failed to test existence for user: " + accountId + "!", e));
    }

    @DELETE
    @Path("/accounts/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeAccount(@PathParam("id") String accountId) {
        AccountAdminResourceConfig cfg = m_config;

        if (isEmpty(accountId)) {
            m_log.log(LOG_INFO, "Failed to process credentials reset request for user: no/invalid accountId!");

            return respondFailure(BAD_REQUEST, CODE_BAD_REQUEST);
        }

        return runInSafetyContext(() -> {
            Account account = m_accountAdmin.removeAccount(accountId);

            return respondSuccess(account, cfg.getEmailKey(), cfg.getAccountRemovedPage(), ACTION_ACCOUNT_REMOVED);
        }, e -> m_log.log(LOG_INFO, "Failed to remove account for user: " + accountId + "!", e));
    }

    @POST
    @Path("/credentialReset")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response requestCredentialResetFormPost(@FormParam(ACCOUNT_ID) String accountId) {
        AccountAdminResourceConfig cfg = m_config;
        return requestCredentialReset(cfg, accountId);
    }

    @POST
    @Path("/credentialReset")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response requestCredentialResetJsonPost(Map<String, String> params) {
        AccountAdminResourceConfig cfg = m_config;
        return requestCredentialReset(cfg, params.get(ACCOUNT_ID));
    }

    @GET
    @Path("/resetCredentials")
    public Response resetCredentials(@QueryParam(ACCESS_TOKEN) String accessToken) {
        AccountAdminResourceConfig cfg = m_config;

        if (isEmpty(accessToken)) {
            m_log.log(LOG_INFO, "Failed to reset credentials for user: no/invalid accountId and/or accessToken!");

            return respondFailure(BAD_REQUEST, CODE_BAD_REQUEST);
        }

        return runInSafetyContext(() -> {
            Account account = m_accountAdmin.getAccountByAccessToken(accessToken);

            String email = account.getCredentials().get(cfg.getEmailKey());

            URI destURI = new URIBuilder(cfg.getChangeCredentialsPage())
                .appendToQuery(ACCESS_TOKEN, accessToken)
                .appendToQuery(ACCOUNT_ID, account.getId())
                .appendToQuery(EMAIL, email)
                .build();

            return respondSuccess(account, cfg.getEmailKey(), Optional.of(destURI), null);
        }, e -> m_log.log(LOG_INFO, "Failed to reset credentials for user!", e));
    }

    @POST
    @Path("/signup")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response signUpFormPost(MultivaluedMap<String, String> formParams) {
        AccountAdminResourceConfig cfg = m_config;
        // We want *all* form parameters, so we cannot use JAXRS annotations here...
        return signUp(cfg, getCredentials(formParams));
    }

    @POST
    @Path("/signup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response signUpJsonPost(Map<String, String> credentials) {
        AccountAdminResourceConfig cfg = m_config;

        return signUp(cfg, credentials);
    }

    @POST
    @Path("/updateCredentials")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCredentialsFormPost(MultivaluedMap<String, String> formParams) {
        AccountAdminResourceConfig cfg = m_config;
        // We want *all* form parameters, so we cannot use JAXRS annotations here...
        return updateCredentials(cfg, getCredentials(formParams));
    }

    @POST
    @Path("/updateCredentials")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCredentialsJsonPost(Map<String, String> newCredentials) {
        AccountAdminResourceConfig cfg = m_config;

        return updateCredentials(cfg, newCredentials);
    }

    @Override
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        AccountAdminResourceConfig newConfig;
        if (properties != null) {
            newConfig = new AccountAdminResourceConfig(properties);
        }
        else {
            newConfig = new AccountAdminResourceConfig();
        }
        m_config = newConfig;
    }

    @POST
    @Path("/verifyAccessToken")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyAccessTokenFormPost(@FormParam(ACCESS_TOKEN) String accessToken) {
        AccountAdminResourceConfig cfg = m_config;

        return verifyAccessToken(cfg, accessToken);
    }

    @POST
    @Path("/verifyAccessToken")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyAccessTokenJsonPost(Map<String, String> params) {
        AccountAdminResourceConfig cfg = m_config;

        return verifyAccessToken(cfg, params.get(ACCESS_TOKEN));
    }

    @GET
    @Path("/verify")
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyAccount(@QueryParam(ACCOUNT_ID) String accountId, @QueryParam(VERIFY_TOKEN) String verifyToken) {
        AccountAdminResourceConfig cfg = m_config;

        if (!cfg.isAllowSignup()) {
            // Users cannot sign up themselves, so they cannot verify as well...
            return respondFailure(NOT_FOUND, CODE_BAD_REQUEST);
        }

        if (isEmpty(accountId) || isEmpty(verifyToken)) {
            m_log.log(LOG_INFO, "Failed to verify account for user: no/invalid accountId and/or verifyToken!");

            return respondFailure(BAD_REQUEST, CODE_BAD_REQUEST);
        }

        return runInSafetyContext(() -> {
            Account account = m_accountAdmin.verifyAccount(accountId, verifyToken);

            m_log.log(LOG_INFO, "Account verified for user: " + account.getId());

            return respondSuccess(account, cfg.getEmailKey(), cfg.getAccountVerifiedPage(), ACTION_ACCOUNT_VERIFIED);
        }, e -> m_log.log(LOG_INFO, "Failed to verify account for user!", e));
    }

    private Response requestCredentialReset(AccountAdminResourceConfig cfg, String accountId) {
        if (isEmpty(accountId)) {
            m_log.log(LOG_INFO, "Failed to process credentials reset request for user: no/invalid accountId!");

            return respondFailure(BAD_REQUEST, CODE_BAD_REQUEST);
        }

        return runInSafetyContext(() -> {
            Account account = m_accountAdmin.resetCredentials(accountId, false /* forced */);

            // Create a URI that callback on this very same endpoint...
            URI resetPasswordURI = URIBuilder.of(m_request)
                .appendToPath("../resetCredentials")
                .appendToQuery(ACCESS_TOKEN, account.getAccessToken().get())
                .build();

            return respondSuccess(account, cfg.getEmailKey(), cfg.getCredentialsResetRequestedPage(), ACTION_RESET_REQUESTED, resetPasswordURI);
        }, e -> m_log.log(LOG_INFO, "Failed to request credentials reset for user: " + accountId + "!", e));
    }

    private Response respondSuccess(Account account, String emailKey, Optional<URI> destURI, String action) {
        return respondSuccess(account, emailKey, destURI, action, null);
    }

    private Response respondSuccess(Account account, String emailKey, Optional<URI> destURI, String action, URI notifyURL) {
        String email = account.getCredentials().get(emailKey);

        Map<String, Object> entity = asMap(KEY_ACCOUNT_EMAIL, email, KEY_ACCOUNT_ID, account.getId(),
            KEY_ACCOUNT_STATE, account.getState().name());
        if (action != null) {
            entity.put(KEY_ACCOUNT_ACTION, action);
        }
        if (notifyURL != null) {
            entity.put(KEY_NOTIFY_URL, notifyURL.toASCIIString());
        }

        if (action != null) {
            m_eventAdmin.postEvent(new Event(TOPIC_NOTIFY_ACCOUNT_OWNER, entity));
        }

        return AccountAdminHelper.respondSuccess(m_request, destURI, action).build();
    }

    private Response signUp(AccountAdminResourceConfig cfg, Map<String, String> credentials) {
        if (!cfg.isAllowSignup()) {
            // Users cannot sign up themselves...
            return respondFailure(NOT_FOUND, CODE_BAD_REQUEST);
        }

        return runInSafetyContext(() -> {
            Account account = m_accountAdmin.createAccount(credentials);

            // Create a URI that callback on this very same endpoint...
            URI verificationURI = URIBuilder.of(m_request)
                .appendToPath("../verify")
                .appendToQuery(VERIFY_TOKEN, account.getAccessToken().get())
                .appendToQuery(ACCOUNT_ID, account.getId())
                .build();

            // Either return an empty OK if no redirect is needed, or redirect to the signup success page...
            return respondSuccess(account, cfg.getEmailKey(), cfg.getSignupSuccessPage(), ACTION_SIGNUP_SUCCESS, verificationURI);
        }, e -> m_log.log(LOG_INFO, "Failed to sign up user!", e));
    }

    private Response updateCredentials(AccountAdminResourceConfig cfg, Map<String, String> newCredentials) {
        return runInSafetyContext(() -> {
            Account account;
            Map<String, String> oldCredentials = null;

            String accessToken = newCredentials.remove(ACCESS_TOKEN);
            if (!isEmpty(accessToken)) {
                account = m_accountAdmin.updateAccount(newCredentials, accessToken);
            }
            else {
                // Check if both the old and new credentials are present...
                oldCredentials = splitOldAndNewCredentials(newCredentials);
                if (oldCredentials == null) {
                    m_log.log(LOG_INFO, "Failed to update credentials for user: no/invalid accessToken or old credentials not supplied!");

                    return respondFailure(BAD_REQUEST, CODE_BAD_REQUEST);
                }

                account = m_accountAdmin.updateAccount(oldCredentials, newCredentials);
            }

            return respondSuccess(account, cfg.getEmailKey(), cfg.getCredentialsChangedPage(), ACTION_CREDENTIALS_UPDATED);
        }, e -> m_log.log(LOG_INFO, "Failed to update credentials for user!", e));

    }

    private Response verifyAccessToken(AccountAdminResourceConfig cfg, String accessToken) {
        if (isEmpty(accessToken)) {
            m_log.log(LOG_INFO, "Failed to verify access token for user: no/invalid accessToken!");

            return respondFailure(BAD_REQUEST, CODE_BAD_REQUEST);
        }

        return runInSafetyContext(() -> {
            Account account = m_accountAdmin.getAccountByAccessToken(accessToken);

            return respondSuccess(account, cfg.getEmailKey(), Optional.empty(), ACTION_ACCOUNT_VERIFIED);
        }, e -> m_log.log(LOG_INFO, "Failed to verify access token for user!", e));
    }
}
