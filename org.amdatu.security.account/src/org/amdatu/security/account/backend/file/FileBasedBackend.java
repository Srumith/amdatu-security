/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.backend.file;

import static org.amdatu.security.account.backend.file.IOUtils.readAccount;
import static org.amdatu.security.account.backend.file.IOUtils.writeAccount;
import static org.osgi.service.log.LogService.LOG_INFO;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.DestroyFailedException;

import org.amdatu.security.account.Account;
import org.amdatu.security.account.AccountAdminBackend;
import org.amdatu.security.account.AccountExistsException;
import org.amdatu.security.account.NoSuchAccountException;
import org.amdatu.security.account.backend.file.IOUtils.HmacInputStream;
import org.amdatu.security.account.backend.file.IOUtils.HmacOutputStream;
import org.apache.felix.dm.Component;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.log.LogService;

/**
 * Provides a file-based {@link AccountAdminBackend} that stores account information in a file in the bundle's data area.
 */
public class FileBasedBackend implements AccountAdminBackend {
    private static final int FILE_MAGIC = 0x41434301;

    private final ReadWriteLock m_lock = new ReentrantReadWriteLock(false);
    private final Map<String, Account> m_accounts = new HashMap<>();
    // Managed by Felix DM...
    private volatile BundleContext m_context;
    private volatile LogService m_log;
    // Locally managed...
    private volatile SecretKey m_secretKey;

    /**
     * Creates a new {@link FileBasedBackend} instance.
     */
    public FileBasedBackend() {
        super();
    }

    /**
     * Creates a new {@link FileBasedBackend} instance.
     */
    protected FileBasedBackend(SecretKey secretKey) {
        m_secretKey = secretKey;
    }

    @Override
    public boolean accountExists(String accountId) {
        return protectedRead(() -> m_accounts.containsKey(accountId));
    }

    @Override
    public void create(Account account) throws AccountExistsException {
        protectedWrite(() -> {
            String accountId = account.getId();
            if (m_accounts.putIfAbsent(accountId, account) != null) {
                m_log.log(LOG_INFO, String.format("Account NOT created for %s: account already exists...", accountId));

                throw new AccountExistsException(accountId);
            }

            m_log.log(LOG_INFO, String.format("Account created for %s successfully...", accountId));

            return account;
        });
    }

    @Override
    public Optional<Account> get(String accountId) {
        return protectedRead(() -> {
            Account account = m_accounts.get(accountId);
            return Optional.ofNullable(account);
        });
    }

    @Override
    public void remove(Account account) {
        protectedWrite(() -> {
            String id = account.getId();
            if (!m_accounts.containsKey(id)) {
                m_log.log(LOG_INFO, String.format("Account NOT removed for %s: no such account exists...", id));

                throw new NoSuchAccountException();
            }

            m_log.log(LOG_INFO, String.format("Account removed for %s...", id));

            return m_accounts.remove(id);
        });
    }

    @Override
    public void update(Account account) {
        protectedWrite(() -> {
            String id = account.getId();
            if (!m_accounts.containsKey(id)) {
                m_log.log(LOG_INFO, String.format("Account NOT updated for %s: no such account exists...", id));

                throw new NoSuchAccountException();
            }

            m_log.log(LOG_INFO, String.format("Account updated for %s...", id));

            return m_accounts.put(id, new Account(account));
        });
    }

    /**
     * Called by Felix DM.
     */
    public void updated(FileBasedBackendConfig config) throws ConfigurationException {
        destroySecretKey();

        String alg = config.getSecretKeyAlgorithm();
        if (alg == null || "".equals(alg)) {
            throw new ConfigurationException("secretKeyAlgorithm", "missing or invalid value!");
        }

        String keyURIStr = config.getSecretKeyURI();
        if (keyURIStr == null || "".equals(keyURIStr)) {
            throw new ConfigurationException("secretKeyURI", "missing or invalid value!");
        }

        URI keyURI;
        try {
            keyURI = new URI(keyURIStr);
        }
        catch (URISyntaxException e) {
            throw new ConfigurationException("secretKeyURI", "missing or invalid value!");
        }

        byte[] keyData;
        if ("data".equals(keyURI.getScheme())) {
            keyData = Base64.getDecoder().decode(keyURI.getSchemeSpecificPart());
        }
        else {
            throw new ConfigurationException("secretKeyURI", "missing or invalid scheme!");
        }

        m_secretKey = new SecretKeySpec(keyData, alg);
    }

    /**
     * Clears this backend.
     */
    protected final void clear() {
        protectedWrite(() -> {
            m_accounts.clear();
            return null;
        });
    }

    protected void readAllAccounts(File file) throws IOException, GeneralSecurityException {
        Map<String, Account> newAccounts = readAccounts(file);
        m_accounts.clear();
        m_accounts.putAll(newAccounts);
    }

    /**
     * @return the number of accounts in this backend.
     */
    protected final int size() {
        return protectedRead(m_accounts::size);
    }

    /**
     * Called by Felix DM when starting this component.
     */
    protected final void start(Component comp) throws Exception {
        File f = m_context.getDataFile("accounts.dat");
        if (f == null || !f.exists()) {
            return;
        }

        protectedWrite(() -> {
            try {
                readAllAccounts(f);
            }
            catch (Exception e) {
                m_log.log(LOG_WARNING, "Failed to read all accounts!", e);
            }
            return null;
        });
    }

    /**
     * Called by Felix DM when stopping this component.
     */
    protected final void stop(Component comp) throws Exception {
        File f = m_context.getDataFile("accounts.dat");
        if (f == null || !f.canRead()) {
            return;
        }

        protectedWrite(() -> {
            try {
                writeAllAccounts(f);
            }
            catch (Exception e) {
                m_log.log(LOG_WARNING, "Failed to write all accounts!", e);
            }
            return null;
        });
    }

    protected void writeAllAccounts(File file) throws IOException, GeneralSecurityException {
        writeAccounts(file, m_accounts);
    }

    private void destroySecretKey() {
        try {
            if (m_secretKey != null) {
                m_secretKey.destroy();
            }
        }
        catch (DestroyFailedException e) {
            m_log.log(LOG_WARNING, "Failed to destroy secret key!", e);
        }
    }

    private <RT> RT protectedRead(Supplier<RT> supplier) {
        m_lock.readLock().lock();
        try {
            return supplier.get();
        }
        finally {
            m_lock.readLock().unlock();
        }
    }

    private <RT> RT protectedWrite(Supplier<RT> supplier) {
        m_lock.writeLock().lock();
        try {
            return supplier.get();
        }
        finally {
            m_lock.writeLock().unlock();
        }
    }

    private Map<String, Account> readAccounts(File file) throws IOException, GeneralSecurityException {
        try (FileInputStream fis = new FileInputStream(file);
                        HmacInputStream his = new HmacInputStream(fis, m_secretKey);
                        DataInputStream dis = new DataInputStream(his)) {
            int magic = dis.readInt();
            if (magic != FILE_MAGIC) {
                throw new IOException("Invalid accounts file: unexpected file magic!");
            }

            int entryCount = dis.readInt();
            Map<String, Account> result = new HashMap<>(entryCount);

            for (int i = 0; i < entryCount; i++) {
                String accountId = dis.readUTF();
                Account account = readAccount(dis);

                result.put(accountId, account);
            }

            if (result.size() != entryCount) {
                throw new IOException("Invalid accounts file: not all accounts were read?!");
            }

            byte[] actualMAC = his.getMAC();

            int len = dis.readInt();
            if (len < 0 || len != actualMAC.length) {
                throw new IOException("Invalid accounts file: invalid MAC length!");
            }

            byte[] expectedMAC = new byte[len];
            dis.readFully(expectedMAC);

            if (!MessageDigest.isEqual(actualMAC, expectedMAC)) {
                throw new IOException("Invalid accounts file: HMAC does not match?!");
            }

            return result;
        }
    }

    private void writeAccounts(File file, Map<String, Account> accounts) throws IOException, GeneralSecurityException {
        try (FileOutputStream fos = new FileOutputStream(file);
                        HmacOutputStream hos = new HmacOutputStream(fos, m_secretKey);
                        DataOutputStream dos = new DataOutputStream(hos)) {
            dos.writeInt(FILE_MAGIC);
            dos.writeInt(accounts.size());

            for (Map.Entry<String, Account> entry : accounts.entrySet()) {
                dos.writeUTF(entry.getKey());
                writeAccount(entry.getValue(), dos);
            }

            // Write the HMAC as last part...
            byte[] mac = hos.getMAC();

            dos.writeInt(mac.length);
            dos.write(mac);
        }
    }
}
