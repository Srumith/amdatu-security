/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin;

import static org.amdatu.security.account.admin.AccountAdminConfig.KEY_ACCOUNT_REMOVAL_ALLOWED;
import static org.amdatu.security.account.admin.AccountAdminConfig.KEY_ACCOUNT_VERIFICATION_NEEDED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;

import org.amdatu.security.account.Account;
import org.amdatu.security.account.Account.State;
import org.amdatu.security.account.AccountAdminBackend;
import org.amdatu.security.account.AccountCredentialResetException;
import org.amdatu.security.account.AccountException;
import org.amdatu.security.account.AccountLockedException;
import org.amdatu.security.account.AccountValidationException;
import org.amdatu.security.account.AccountValidator;
import org.amdatu.security.account.NoSuchAccountException;
import org.amdatu.security.account.TestPasswordHasher;
import org.amdatu.security.account.UnverifiedAccountException;
import org.amdatu.security.password.hash.PasswordHasher;
import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.InvalidTokenException.Reason;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogService;

/**
 * Test cases for {@link AccountAdminImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountAdminImplTest {
    private static final String SECRET_PLAIN = "secret";
    private static final String SECRET_SHA256 = "$5$K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=";

    private static final String PASSWORD_PLAIN = "password";
    private static final String PASSWORD_SHA256 = "$5$XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=";

    private static final String EXISTING_ACCOUNT_ID = "existing_user@amdatu.org";
    private static final String NEW_ACCOUNT_ID = "new_user@amdatu.org";
    private static final String LOCKED_ACCOUNT_ID = "bad_user@amdatu.org";
    private static final String NON_EXISTING_ACCOUNT_ID = "does_not_exist@amdatu.org";

    private PasswordHasher m_pwdHasher = new TestPasswordHasher();
    @Spy
    private AccountAdminBackend m_backend = new MockAccountAdminBackend();
    @Mock
    private AccountValidator m_accountValidator;
    @Mock
    private EventAdmin m_eventAdmin;
    @Mock
    private LogService m_logService;
    @Mock
    private TokenProvider m_tokenProvider;
    @InjectMocks
    private AccountAdminImpl m_accountAdmin = new AccountAdminImpl(m_backend, m_pwdHasher);

    @Rule
    public final ExpectedException m_rule = ExpectedException.none();

    private static Dictionary<String, Object> asDictionary(String... entries) {
        Dictionary<String, Object> result = new Hashtable<>();
        for (int i = 0; i < entries.length; i += 2) {
            result.put(entries[i], entries[i + 1]);
        }
        return result;
    }

    private static Map<String, String> asMap(String... entries) {
        Map<String, String> result = new HashMap<>();
        for (int i = 0; i < entries.length; i += 2) {
            result.put(entries[i], entries[i + 1]);
        }
        return result;
    }

    private static Map<String, String> createCredentials(String email, String password) {
        return asMap("email", email, "password", password);
    }

    @Before
    @SuppressWarnings("unchecked")
    public void setUp() throws Exception {
        when(m_tokenProvider.generateToken(anyMap())).thenAnswer(mock -> {
            String sub = ((Map<String, String>) mock.getArguments()[0]).get("sub");
            return sub;
        });
        when(m_tokenProvider.verifyToken(anyString())).thenAnswer(mock -> {
            String token = mock.getArguments()[0].toString();
            if (EXISTING_ACCOUNT_ID.equals(token) || NEW_ACCOUNT_ID.equals(token) || LOCKED_ACCOUNT_ID.equals(token)) {
                return asMap("sub", token);
            }
            throw new InvalidTokenException(Reason.UNKNOWN, "invalid token!", null);
        });

        // Inject a couple of accounts with specific properties...
        Map<String, String> creds = createCredentials(EXISTING_ACCOUNT_ID, m_pwdHasher.hash(SECRET_PLAIN));
        m_backend.update(new Account(EXISTING_ACCOUNT_ID, creds, State.NORMAL, null, false /* locked */));

        creds = createCredentials(LOCKED_ACCOUNT_ID, m_pwdHasher.hash("bad"));
        m_backend.update(new Account(LOCKED_ACCOUNT_ID, creds, State.NORMAL, null, true /* locked */));

        when(m_backend.accountExists(EXISTING_ACCOUNT_ID)).thenReturn(Boolean.TRUE);

        m_accountAdmin.updated(null); // ensure we've got a default configuration...

        reset(m_backend); // make sure we've got a clear state...
    }

    @Test
    public void testAccountExists() throws Exception {
        assertTrue(m_accountAdmin.accountExists(EXISTING_ACCOUNT_ID));
        assertFalse(m_accountAdmin.accountExists(NON_EXISTING_ACCOUNT_ID));
        assertFalse(m_accountAdmin.accountExists(null));
    }

    @Test
    public void testCreateAccount() throws Exception {
        Account account = m_accountAdmin.createAccount(createCredentials(NEW_ACCOUNT_ID, SECRET_PLAIN));
        assertNotNull(account);

        assertEquals(NEW_ACCOUNT_ID, account.getId());
        assertEquals(NEW_ACCOUNT_ID, account.getCredentials().get("email"));
        assertEquals(SECRET_SHA256, account.getCredentials().get("password"));
        assertEquals(State.ACCOUNT_VERIFICATION_NEEDED, account.getState());
        assertEquals(Optional.of(NEW_ACCOUNT_ID), account.getAccessToken());
    }

    @Test
    public void testCreateAccount_AccountExists() throws Exception {
        m_rule.expect(AccountException.class);

        assertNull(m_accountAdmin.createAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)));
    }

    @Test
    public void testCreateAccount_AccountValidationFails() throws Exception {
        doThrow(new AccountValidationException()).when(m_accountValidator).validate(any());

        m_rule.expect(AccountValidationException.class);

        assertNull(m_accountAdmin.createAccount(createCredentials(NEW_ACCOUNT_ID, PASSWORD_PLAIN)));
    }

    @Test
    public void testCreateAccount_EmptyCredentials() throws Exception {
        m_rule.expect(AccountException.class);

        assertNull(m_accountAdmin.createAccount(asMap()));
    }

    @Test
    public void testCreateAccount_MissingAccountId() throws Exception {
        m_rule.expect(AccountException.class);

        assertNull(m_accountAdmin.createAccount(asMap("password", "secret")));
    }

    @Test
    public void testCreateAccount_MissingPassword() throws Exception {
        m_rule.expect(AccountException.class);

        assertNull(m_accountAdmin.createAccount(asMap("email", EXISTING_ACCOUNT_ID)));
    }

    @Test
    public void testCreateAccount_NoVerificationNeeded() throws Exception {
        m_accountAdmin.updated(asDictionary(KEY_ACCOUNT_VERIFICATION_NEEDED, "false"));

        Account account = m_accountAdmin.createAccount(createCredentials(NEW_ACCOUNT_ID, PASSWORD_PLAIN));
        assertNotNull(account);

        assertEquals(NEW_ACCOUNT_ID, account.getId());
        assertEquals(NEW_ACCOUNT_ID, account.getCredentials().get("email"));
        assertEquals(PASSWORD_SHA256, account.getCredentials().get("password"));
        assertEquals(State.NORMAL, account.getState());
        assertEquals(Optional.of(NEW_ACCOUNT_ID), account.getAccessToken());
    }

    @Test
    public void testGetAccount() throws Exception {
        Optional<Account> account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN));
        assertNotNull(account);

        assertTrue(account.isPresent());
    }

    @Test
    public void testGetAccount_ForcedCredentialsReset() throws Exception {
        m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, true /* force */);

        m_rule.expect(AccountCredentialResetException.class);

        assertNull(m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)));
    }

    @Test
    public void testGetAccount_IncorrectCredentials() throws Exception {
        Optional<Account> account;

        // Wrong password...
        account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, "incorrect"));
        assertNotNull(account);

        assertFalse(account.isPresent());

        // Wrong email address...
        account = m_accountAdmin.getAccount(createCredentials(NON_EXISTING_ACCOUNT_ID, SECRET_PLAIN));
        assertNotNull(account);

        assertFalse(account.isPresent());
    }

    @Test
    public void testGetAccount_LockedAccount() throws Exception {
        m_rule.expect(AccountLockedException.class);

        assertNull(m_accountAdmin.getAccount(createCredentials(LOCKED_ACCOUNT_ID, "bad")));
    }

    @Test
    public void testGetAccount_NonExistingAccount() throws Exception {
        Optional<Account> account = m_accountAdmin.getAccount(createCredentials(NON_EXISTING_ACCOUNT_ID, SECRET_PLAIN));
        assertNotNull(account);

        assertFalse(account.isPresent());
    }

    @Test
    public void testGetAccount_UnverifiedAccount() throws Exception {
        m_accountAdmin.createAccount(createCredentials(NEW_ACCOUNT_ID, SECRET_PLAIN));

        m_rule.expect(UnverifiedAccountException.class);

        assertNull(m_accountAdmin.getAccount(createCredentials(NEW_ACCOUNT_ID, SECRET_PLAIN)));
    }

    @Test
    public void testGetAccount_WithAccessToken() throws Exception {
        Account account = m_accountAdmin.createAccount(createCredentials(NEW_ACCOUNT_ID, PASSWORD_PLAIN));
        assertNotNull(account);

        String token = account.getAccessToken().get(); // cannot fail!

        account = m_accountAdmin.getAccountByAccessToken(token);

        assertNotNull(account);
    }

    @Test
    public void testGetAccount_WithAccessTokenForAccountWithoutToken() throws Exception {
        m_rule.expect(NoSuchAccountException.class);

        assertNull(m_accountAdmin.getAccountByAccessToken(EXISTING_ACCOUNT_ID));
    }

    @Test
    public void testGetAccount_WithAccessTokenForLockedAccount() throws Exception {
        m_rule.expect(AccountLockedException.class);

        assertNull(m_accountAdmin.getAccountByAccessToken(LOCKED_ACCOUNT_ID));
    }

    @Test
    public void testGetAccount_WithInvalidAccessToken() throws Exception {
        m_rule.expect(NoSuchAccountException.class);

        assertNull(m_accountAdmin.getAccountByAccessToken("someToken"));
    }

    @Test
    public void testGetAccount_WithPartialMissingCredentials() throws Exception {
        Map<String, String> creds;

        creds = asMap("email", EXISTING_ACCOUNT_ID, "foo", "bar");

        assertFalse(m_accountAdmin.getAccount(creds).isPresent());

        creds = asMap("password", "bar", "foo", "bar");

        assertFalse(m_accountAdmin.getAccount(creds).isPresent());

        creds = asMap("foo", "bar");

        assertFalse(m_accountAdmin.getAccount(creds).isPresent());

        creds = asMap();

        assertFalse(m_accountAdmin.getAccount(creds).isPresent());
    }

    @Test
    public void testGetAccount_WithSuperfluousCredentials() throws Exception {
        Map<String, String> creds = createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN);
        // Add some additional credentials that aren't present in the stored account...
        creds.put("type", "local");
        creds.put("other", "token");

        Optional<Account> account = m_accountAdmin.getAccount(creds);
        assertNotNull(account);

        assertTrue(account.isPresent());
    }

    @Test
    public void testRemoveAccount() throws Exception {
        m_accountAdmin.updated(asDictionary(KEY_ACCOUNT_REMOVAL_ALLOWED, "true"));

        Account account = m_accountAdmin.removeAccount(EXISTING_ACCOUNT_ID);
        assertNotNull(account);

        assertEquals(EXISTING_ACCOUNT_ID, account.getId());
        assertEquals(EXISTING_ACCOUNT_ID, account.getCredentials().get("email"));
    }

    @Test
    public void testRemoveAccount_NonExistingAccount() throws Exception {
        m_accountAdmin.updated(asDictionary(KEY_ACCOUNT_REMOVAL_ALLOWED, "true"));

        m_rule.expect(NoSuchAccountException.class);

        assertNull(m_accountAdmin.removeAccount(NON_EXISTING_ACCOUNT_ID));
    }

    @Test
    public void testRemoveAccount_NotAllowed() throws Exception {
        m_accountAdmin.updated(asDictionary(KEY_ACCOUNT_REMOVAL_ALLOWED, "false"));

        m_rule.expect(AccountException.class);

        assertNull(m_accountAdmin.removeAccount(EXISTING_ACCOUNT_ID));
    }

    @Test
    public void testRemoveAccount_NotAllowedByBackend() throws Exception {
        m_accountAdmin.updated(asDictionary(KEY_ACCOUNT_REMOVAL_ALLOWED, "true"));
        ((MockAccountAdminBackend) m_backend).setRemovalSupported(false);

        m_rule.expect(AccountException.class);

        assertNull(m_accountAdmin.removeAccount(EXISTING_ACCOUNT_ID));
    }

    @Test
    public void testResetCredentials_Forced() throws Exception {
        Account account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, true /* forced */);

        assertEquals(EXISTING_ACCOUNT_ID, account.getAccessToken().get());

        assertEquals(State.FORCED_CREDENTIALS_RESET, account.getState());

        verify(m_backend).update(account);
    }

    @Test
    public void testResetCredentials_ForcedFlagNotCleared() throws Exception {
        Account account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, true /* forced */);

        assertEquals(EXISTING_ACCOUNT_ID, account.getAccessToken().get());

        assertEquals(State.FORCED_CREDENTIALS_RESET, account.getState());

        m_rule.expect(AccountCredentialResetException.class);

        assertNull(m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)));
    }

    @Test
    public void testResetCredentials_ForcedTwice() throws Exception {
        Account account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, true /* forced */);

        assertTrue(account.getAccessToken().isPresent());

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, true /* forced */);

        assertEquals(State.FORCED_CREDENTIALS_RESET, account.getState());

        verify(m_backend, atLeast(2)).update(account);
    }

    @Test
    public void testResetCredentials_LockedAccount() throws Exception {
        m_rule.expect(AccountLockedException.class);

        assertNull(m_accountAdmin.resetCredentials(LOCKED_ACCOUNT_ID, false /* forced */));
    }

    @Test
    public void testResetCredentials_NonExistingAccount() throws Exception {
        m_rule.expect(NoSuchAccountException.class);

        assertNull(m_accountAdmin.resetCredentials(NON_EXISTING_ACCOUNT_ID, false /* forced */));
    }

    @Test
    public void testResetCredentials_RequestForcedResetAfterVoluntaryReset() throws Exception {
        Account account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);

        assertTrue(account.getAccessToken().isPresent());
        assertEquals(State.VOLUNTARY_CREDENTIALS_RESET, account.getState());

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, true /* forced */);

        assertEquals(State.FORCED_CREDENTIALS_RESET, account.getState());

        verify(m_backend, atLeast(2)).update(account);
    }

    @Test
    public void testResetCredentials_RequestVoluntaryResetAfterForcedReset() throws Exception {
        Account account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, true /* forced */);

        assertTrue(account.getAccessToken().isPresent());
        assertEquals(State.FORCED_CREDENTIALS_RESET, account.getState());

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);

        assertEquals(State.FORCED_CREDENTIALS_RESET, account.getState());

        verify(m_backend, atLeast(2)).update(account);
    }

    @Test
    public void testResetCredentials_Twice() throws Exception {
        Account account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);

        assertTrue(account.getAccessToken().isPresent());
        assertEquals(State.VOLUNTARY_CREDENTIALS_RESET, account.getState());

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);

        assertEquals(State.VOLUNTARY_CREDENTIALS_RESET, account.getState());

        verify(m_backend, atLeast(2)).update(account);
    }

    @Test
    public void testResetCredentials_UnverifiedAccount() throws Exception {
        Account account = m_accountAdmin.createAccount(createCredentials(NEW_ACCOUNT_ID, SECRET_PLAIN));
        assertNotNull(account);

        m_rule.expect(AccountCredentialResetException.class);

        assertNull(m_accountAdmin.resetCredentials(NEW_ACCOUNT_ID, true /* forced */));
    }

    @Test
    public void testResetCredentials_Voluntary() throws Exception {
        Account account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        assertFalse(account.getAccessToken().isPresent());

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);

        assertTrue(account.getAccessToken().isPresent());
        assertEquals(State.VOLUNTARY_CREDENTIALS_RESET, account.getState());

        verify(m_backend).update(account);
    }

    @Test
    public void testResetCredentials_VoluntaryFlagClearedAfterSuccessfulGet() throws Exception {
        Account account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);

        assertEquals(State.VOLUNTARY_CREDENTIALS_RESET, account.getState());

        account = m_accountAdmin.getAccount(createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN)).get();

        verify(m_backend, atLeast(2)).update(account);

        assertEquals(State.NORMAL, account.getState());
    }

    @Test
    public void testUpdateAccount_AccountValidationFails() throws Exception {
        Map<String, String> creds = createCredentials(EXISTING_ACCOUNT_ID, "secret");

        doThrow(new AccountValidationException()).when(m_accountValidator).validate(any());

        m_rule.expect(AccountValidationException.class);

        assertNull(m_accountAdmin.updateAccount(creds, creds));
    }

    @Test
    public void testUpdateAccount_LockedAccount() throws Exception {
        Map<String, String> creds = createCredentials(LOCKED_ACCOUNT_ID, "bad");

        m_rule.expect(AccountLockedException.class);

        assertNull(m_accountAdmin.updateAccount(creds, creds));
    }

    @Test
    public void testUpdateAccount_NonExistingAccount() throws Exception {
        m_rule.expect(NoSuchAccountException.class);

        Map<String, String> creds = createCredentials(NON_EXISTING_ACCOUNT_ID, "bad");

        assertNull(m_accountAdmin.updateAccount(creds, creds));
    }

    @Test
    public void testUpdateAccount_NoResetTokenRequested() throws Exception {
        m_rule.expect(NoSuchAccountException.class);

        assertNull(m_accountAdmin.updateAccount(createCredentials(EXISTING_ACCOUNT_ID, "password"), EXISTING_ACCOUNT_ID));
    }

    @Test
    public void testUpdateAccount_WithInvalidResetToken() throws Exception {
        Account account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);
        assertTrue(account.getAccessToken().isPresent());

        m_rule.expect(NoSuchAccountException.class);

        assertNull(m_accountAdmin.updateAccount(createCredentials(EXISTING_ACCOUNT_ID, "password"), "invalidToken"));
    }

    @Test
    public void testUpdateAccount_WithoutResetToken() throws Exception {
        Map<String, String> oldCreds = createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN);
        Map<String, String> newCreds = createCredentials(EXISTING_ACCOUNT_ID, PASSWORD_PLAIN);

        Account account = m_accountAdmin.updateAccount(oldCreds, newCreds);
        assertNotNull(account);

        assertEquals(EXISTING_ACCOUNT_ID, account.getId());
        assertEquals(EXISTING_ACCOUNT_ID, account.getCredentials().get("email"));
        assertEquals(PASSWORD_SHA256, account.getCredentials().get("password"));
        assertEquals(State.NORMAL, account.getState());
        assertEquals(Optional.empty(), account.getAccessToken());
    }

    @Test
    public void testUpdateAccount_WithoutResetToken_ForcedUpdate() throws Exception {
        Account account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, true /* forced */);
        assertTrue(account.getAccessToken().isPresent());

        Map<String, String> oldCreds = createCredentials(EXISTING_ACCOUNT_ID, SECRET_PLAIN);
        Map<String, String> newCreds = createCredentials(EXISTING_ACCOUNT_ID, "password");

        m_rule.expect(AccountCredentialResetException.class);

        assertNotNull(m_accountAdmin.updateAccount(oldCreds, newCreds));
    }

    @Test
    public void testUpdateAccount_WithoutResetToken_InvalidOldCredentials() throws Exception {
        Map<String, String> oldCreds = createCredentials(EXISTING_ACCOUNT_ID, "incorrect");
        Map<String, String> newCreds = createCredentials(EXISTING_ACCOUNT_ID, "password");

        m_rule.expect(NoSuchAccountException.class);

        assertNull(m_accountAdmin.updateAccount(oldCreds, newCreds));
    }

    @Test
    public void testUpdateAccount_WithResetToken() throws Exception {
        Account account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);
        assertTrue(account.getAccessToken().isPresent());

        account = m_accountAdmin.updateAccount(createCredentials(EXISTING_ACCOUNT_ID, PASSWORD_PLAIN), EXISTING_ACCOUNT_ID);

        assertEquals(EXISTING_ACCOUNT_ID, account.getId());
        assertEquals(EXISTING_ACCOUNT_ID, account.getCredentials().get("email"));
        assertEquals(PASSWORD_SHA256, account.getCredentials().get("password"));
        assertEquals(State.NORMAL, account.getState());
        assertEquals(Optional.empty(), account.getAccessToken());
    }

    @Test
    public void testUpdateAccount_WithResetToken_AccountValidationFails() throws Exception {
        Account account = m_accountAdmin.resetCredentials(EXISTING_ACCOUNT_ID, false /* forced */);
        assertTrue(account.getAccessToken().isPresent());

        doThrow(new AccountValidationException()).when(m_accountValidator).validate(any());

        m_rule.expect(AccountValidationException.class);

        assertNull(m_accountAdmin.updateAccount(createCredentials(EXISTING_ACCOUNT_ID, "password"), EXISTING_ACCOUNT_ID));
    }

    @Test
    public void testUpdateAccount_WithResetToken_LockedAccount() throws Exception {
        m_rule.expect(AccountLockedException.class);

        assertNull(m_accountAdmin.updateAccount(createCredentials(LOCKED_ACCOUNT_ID, "password"), LOCKED_ACCOUNT_ID));
    }

    @Test
    public void testVerifyAccount() throws Exception {
        Account account = m_accountAdmin.createAccount(createCredentials(NEW_ACCOUNT_ID, SECRET_PLAIN));
        assertNotNull(account);

        account = m_accountAdmin.verifyAccount(NEW_ACCOUNT_ID, NEW_ACCOUNT_ID);
        assertNotNull(account);

        assertEquals(NEW_ACCOUNT_ID, account.getId());
        assertEquals(NEW_ACCOUNT_ID, account.getCredentials().get("email"));
        assertEquals(SECRET_SHA256, account.getCredentials().get("password"));
        assertEquals(State.NORMAL, account.getState());
        assertEquals(Optional.empty(), account.getAccessToken());
    }

    @Test
    public void testVerifyAccount_WithInvalidVerificationToken() throws Exception {
        Account account = m_accountAdmin.createAccount(createCredentials(NEW_ACCOUNT_ID, SECRET_PLAIN));
        assertNotNull(account);

        m_rule.expect(AccountException.class);

        assertNull(m_accountAdmin.verifyAccount(NEW_ACCOUNT_ID, "invalidToken"));
    }

    @Test
    public void testVerifyAccount_WithInvalidVerificationToken2() throws Exception {
        m_rule.expect(AccountException.class);

        assertNull(m_accountAdmin.verifyAccount(EXISTING_ACCOUNT_ID, EXISTING_ACCOUNT_ID));
    }

    @Test
    public void testVerifyAccount_WithResetToken_LockedAccount() throws Exception {
        m_rule.expect(AccountLockedException.class);

        assertNull(m_accountAdmin.verifyAccount(LOCKED_ACCOUNT_ID, LOCKED_ACCOUNT_ID));
    }
}
