/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rest.impl;

import static org.amdatu.security.authorization.InvokeAction.INVOKE_ACTION;
import static org.amdatu.security.authorization.InvokeAction.INVOKE_METHOD_NAME;
import static org.amdatu.security.authorization.InvokeAction.INVOKE_METHOD_TO_GENERIC_STRING;
import static org.amdatu.security.authorization.InvokeAction.INVOKE_METHOD_TO_STRING;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;

import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.security.authorization.BreadAction;
import org.amdatu.security.authorization.annotation.AddAction;
import org.amdatu.security.authorization.annotation.BrowseAction;
import org.amdatu.security.authorization.annotation.DeleteAction;
import org.amdatu.security.authorization.annotation.EditAction;
import org.amdatu.security.authorization.annotation.ReadAction;
import org.amdatu.security.authorization.annotation.ResourceAttribute;
import org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor;
import org.amdatu.web.rest.jaxrs.RequestContext;

/**
 * Provides a {@link JaxRsRequestInterceptor} that checks whether a user is authorized to invoke a method on a RESTful web service.
 */
public class AuthorizationRequestInterceptor implements JaxRsRequestInterceptor {

    private static final String SUBJECT = "rest-subject";
    private volatile AuthorizationService m_authorizationService;

    @Override
    public void intercept(RequestContext context) {
        HttpServletRequest request = context.getRequest();

        Map<String, Object> subjectAttributes = new HashMap<>();

        Enumeration<String> attributeNames = request.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String attributeName = attributeNames.nextElement();
            subjectAttributes.put(attributeName, request.getAttribute(attributeName));
        }

        Class<?> resourceClass = context.getResource().getClass();
        String resourceEntityType = resourceClass.getName();

        Map<String, Object> resourceAttributes = createResourceAttributeMap(context);

        boolean authorized;
        authorized = m_authorizationService.isAuthorized(SUBJECT, subjectAttributes, INVOKE_ACTION,
            resourceEntityType, Optional.of(resourceAttributes));

        if (authorized) {
            // Access granted based on role based access rules we're done
            return;
        }

        boolean attributeBasedAuthz = false;
        Method method = context.getMethod();
        Annotation[] annotations = method.getAnnotations();
        for (Annotation annotation : annotations) {
            String annotationResourceEntityType = null;
            BreadAction annotationAction = null;
            if (annotation instanceof BrowseAction) {
                BrowseAction browseAction = (BrowseAction) annotation;
                annotationResourceEntityType = browseAction.value();
                annotationAction = BreadAction.BROWSE;
            } else if (annotation instanceof ReadAction) {
                ReadAction readAction = (ReadAction) annotation;
                annotationResourceEntityType = readAction.value();
                annotationAction = BreadAction.READ;
            } else if (annotation instanceof EditAction) {
                EditAction editAction = (EditAction) annotation;
                annotationResourceEntityType = editAction.value();
                annotationAction = BreadAction.EDIT;
            } else if (annotation instanceof AddAction) {
                AddAction addAction = (AddAction) annotation;
                annotationResourceEntityType = addAction.value();
                annotationAction = BreadAction.ADD;
            } else if (annotation instanceof DeleteAction) {
                DeleteAction deleteAction = (DeleteAction) annotation;
                annotationResourceEntityType = deleteAction.value();
                annotationAction = BreadAction.DELETE;
            }

            if (annotationResourceEntityType != null) {
                m_authorizationService.checkAuthorization(SUBJECT, subjectAttributes, annotationAction,
                    annotationResourceEntityType, Optional.ofNullable(resourceAttributes));
                attributeBasedAuthz = true;
            }
        }

        if (!attributeBasedAuthz) {
            throw new ForbiddenException("No access to " + resourceEntityType);
        }
    }

    static Map<String, Object> createResourceAttributeMap(RequestContext context) {
        Map<String, Object> resourceAttributes = new HashMap<>();

        Method method = context.getMethod();
        resourceAttributes.put(INVOKE_METHOD_TO_GENERIC_STRING, method.toGenericString());
        resourceAttributes.put(INVOKE_METHOD_TO_STRING, method.toString());
        resourceAttributes.put(INVOKE_METHOD_NAME, method.getName());

        Parameter[] parameters = method.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            Parameter parameter = parameters[i];
            ResourceAttribute resourceAttributeAnnotation = parameter.getAnnotation(ResourceAttribute.class);
            if (resourceAttributeAnnotation != null) {
                String attributeKey = resourceAttributeAnnotation.value();
                Object attributeValue = context.getParams()[i];

                resourceAttributes.put(attributeKey, attributeValue);
            }
        }
        return resourceAttributes;
    }

}
