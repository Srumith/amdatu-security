/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.local.impl;

import static java.util.stream.Collectors.toMap;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_ACCESS_DENIED;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_INVALID_REQUEST_URI;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_SERVER_ERROR;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_UNAUTHORIZED_CLIENT;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.ERROR_CODE;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.LOCAL_ID;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_NAME;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_TYPE;
import static org.amdatu.security.authentication.idprovider.local.impl.LocalIdProviderConfig.TYPE_LOCAL;
import static org.amdatu.security.authentication.idprovider.local.impl.util.ConfigUtils.getString;
import static org.osgi.service.log.LogService.LOG_INFO;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.net.URI;
import java.time.Instant;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.amdatu.security.authentication.idprovider.IdProvider;
import org.amdatu.security.authentication.idprovider.local.LocalIdCredentialsProvider;
import org.amdatu.web.util.URIBuilder;
import org.apache.felix.dm.Component;
import org.osgi.service.log.LogService;

/**
 * Provides an {@link IdProvider} implementation that is able to act as local identity provider.
 */
public class LocalIdProvider implements IdProvider {

    private final LocalIdProviderConfig m_config;
    // Injected by Felix DM...
    @SuppressWarnings("unused")
    private volatile LocalIdCredentialsProvider m_credentialsProvider;
    @SuppressWarnings("unused")
    private volatile LogService m_log;

    /**
     * Creates a new {@link LocalIdProvider} instance.
     */
    public LocalIdProvider(LocalIdProviderConfig config) {
        m_config = config;
    }

    private static String coerce(String[] input) {
        if (input.length < 1) {
            return "";
        }
        if (input.length == 1) {
            return input[0];
        }
        return String.join(",", input);
    }

    private static Map<String, String> createMapWithError(String error, String providerName) {
        Map<String, String> tokenAttrs = new HashMap<>();
        tokenAttrs.put(PROVIDER_TYPE, TYPE_LOCAL);
        tokenAttrs.put(PROVIDER_NAME, providerName);
        tokenAttrs.put(ERROR_CODE, error);
        return tokenAttrs;
    }

    @Override
    public URI getAuthenticationURI(URI callbackURI, Map<String, String[]> params) {
        LocalIdProviderConfig cfg = m_config;

        Map<String, String> credentials = getCredentials(params);

        URIBuilder authURIBuilder = new URIBuilder(callbackURI);

        try {
            String localId = m_credentialsProvider.getId(credentials)
                .orElseThrow(RuntimeException::new); // caught below!

            // We'll give each caller its own set of secrets, allowing us to counter
            // CORS (Cross Origin Resource Sharing) attacks and minimize the risk of
            // stuff leaking accidentally...
            byte[] code = cfg.generateClientKey();

            // Use a time limited code that can be used to correlate calls between this
            // method and the #getUserIdentity method...
            authURIBuilder.appendToQuery("state", generateStateCode(cfg, localId, code));
            authURIBuilder.appendToQuery("code", encodeCode(code));
        }
        catch (IllegalStateException e) {
            log(LOG_INFO, "Unable to get authentication URI for credentials: "
                + "configuration is invalid!", e);

            authURIBuilder.appendToQuery("error", CODE_SERVER_ERROR);
        }
        catch (Exception e) {
            log(LOG_INFO, "Unable to get authentication URI.", e);

            authURIBuilder.appendToQuery("error", CODE_UNAUTHORIZED_CLIENT);
        }

        return authURIBuilder.build();
    }

    @Override
    public String getType() {
        return TYPE_LOCAL;
    }

    @Override
    public Map<String, String> getUserIdentity(URI callbackURI, Map<String, String[]> params) {
        String error = getString(params, "error");
        String state = getString(params, "state");
        String code = getString(params, "code");

        LocalIdProviderConfig cfg = m_config;
        String providerName = cfg.getName();

        if (error != null) {
            log(LOG_INFO, "Unable to get user identity: error received! [state = %s] [code = %s] [error = %s]",
                state, code, error);

            // This is the easy case...
            return createMapWithError(error, providerName);
        }

        if (state == null || code == null || decodeCode(code) == null) {
            log(LOG_INFO, "Unable to get user identity: no state and/or code! [state = %s] [code = %s]",
                state, code);

            // These two parameters should always be present!
            return createMapWithError(CODE_INVALID_REQUEST_URI, providerName);
        }

        return getTokenAttributesFromRequest(cfg, state, code)
            .orElseGet(() -> createMapWithError(CODE_ACCESS_DENIED, providerName));
    }

    /**
     * Called by Felix DM.
     */
    @SuppressWarnings("unused")
    protected final void start(Component comp) {
        log(LOG_INFO, "Local ID provider '%s' started", m_config.getName());
    }

    /**
     * Called by Felix DM.
     */
    @SuppressWarnings("unused")
    protected final void stop(Component comp) {
        log(LOG_INFO, "Local ID provider '%s' stopped", m_config.getName());
    }

    private byte[] decodeCode(String code) {
        try {
            return Base64.getUrlDecoder().decode(code);
        } catch (IllegalArgumentException e) {
            m_log.log(LOG_INFO, "Could not URLDecode 'code' parameter", e);
            return null;
        }
    }

    private String encodeCode(byte[] code) {
        return Base64.getUrlEncoder().withoutPadding().encodeToString(code);
    }

    private String generateStateCode(LocalIdProviderConfig cfg, String localId, byte[] code) {
        return cfg.generateStateCodeAt(Instant.now(), code, localId);
    }

    private Map<String, String> getCredentials(Map<String, String[]> params) {
        return params.entrySet().stream()
            .collect(toMap(Map.Entry::getKey, e -> coerce(e.getValue())));
    }

    private Optional<Map<String, String>> getTokenAttributesFromRequest(LocalIdProviderConfig cfg, String state, String code) {
        String localId;

        try {
            localId = cfg.verifyStateCodeAt(Instant.now(), decodeCode(code), state);
        }
        catch (Exception e) {
            log(LOG_WARNING, "Verification error while trying to obtain user identity! [state = %s] [code = %s]",
                e, state, code);

            return Optional.empty();
        }

        if (localId == null) {
            log(LOG_WARNING, "Did not get account ID while trying to obtain user identity! [state = %s] [code = %s]",
                state, code);

            return Optional.empty();
        }

        try {
            Map<String, String> tokenAttrs = m_credentialsProvider.getPublicCredentials(localId)
                .map(HashMap::new)
                .orElseThrow(RuntimeException::new); // caught below!

            tokenAttrs.put(PROVIDER_TYPE, TYPE_LOCAL);
            tokenAttrs.put(PROVIDER_NAME, m_config.getName());
            tokenAttrs.put(LOCAL_ID, localId);

            return Optional.of(tokenAttrs);
        }
        catch (Exception e) {
            log(LOG_WARNING, "Failed to obtain account information for %s! Denying access!", e, localId);

            return Optional.empty();
        }
    }

    private void log(int level, String msg, Exception e, Object... args) {
        m_log.log(level, String.format(msg, args), e);
    }

    private void log(int level, String msg, Object... args) {
        log(level, msg, null, args);
    }
}
