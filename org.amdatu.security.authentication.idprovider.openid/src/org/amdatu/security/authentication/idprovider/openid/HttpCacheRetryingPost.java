/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.openid;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openid4java.util.HttpCache;
import org.openid4java.util.HttpRequestOptions;
import org.openid4java.util.HttpResponse;

/**
 * Custom HttpFetcher implementation that retries a POST request once, to solve a particular issue with Steam signature verification.
 * See issue AMDATUSEC-57
 */
public class HttpCacheRetryingPost extends HttpCache {

	private static Log m_log = LogFactory.getLog(HttpCacheRetryingPost.class);

	@Override
	public HttpResponse post(String url, Map<String, String> parameters,
			HttpRequestOptions requestOptions) throws IOException
	{
		HttpResponse httpResponse;
		try {
			httpResponse = super.post(url, parameters, requestOptions);
		} catch (IOException firstFailure) {
			m_log.warn(
					String.format("First POST failed! Retrying one more time: url=%s, parameters=%s, requestOptions=%s",
							url, parameters, requestOptions),
					firstFailure
			);

			try {
				httpResponse = super.post(url, parameters, requestOptions);
			} catch (IOException secondFailure) {
				throw new IOException("Retry of POST also failed", secondFailure);
			}

			m_log.info("Retry of POST succeeded");
		}

		return httpResponse;
	}
}
