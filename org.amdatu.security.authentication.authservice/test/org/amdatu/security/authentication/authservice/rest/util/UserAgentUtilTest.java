package org.amdatu.security.authentication.authservice.rest.util;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UserAgentUtilTest {

	private static final String FIREFOX_DESKTOP_UA = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0";
	private static final String CHROME_DESKTOP_51_UA = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36";
	private static final String CHROME_DESKTOP_67_UA = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";
	private static final String CHROME_DESKTOP_66_UA = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3396.99 Safari/537.36";
	private static final String MACOS_10_14_UA = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Safari/605.1.15";
	private static final String MACOS_10_6_UA = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.52.7 (KHTML, like Gecko) Version/5.1.2 Safari/534.52.7";
	private static final String UC_BROWSER_ANDROID_11_5_UA = "Mozilla/5.0 (Linux; U; Android 6.0.1; zh-CN; F5121 Build/34.0.A.1.247) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.5.1.944 Mobile Safari/537.36";
	private static final String UC_BROWSER_ANDROID_13_1_UA = "Mozilla/5.0 (Linux; U; Android 9; en-US; Redmi Note 8 Pro Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 UCBrowser/13.1.2.1293 Mobile Safari/537.36";
	private static final String SAFARI_12_UA = "Mozilla/5.0 (iPod; CPU iPhone OS 12_0 like macOS) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/12.0 Mobile/14A5335d Safari/602.1.50";

	@Test
	public void testIsSiteNoneCompatible() {
		assertTrue(!UserAgentUtil.isSameSiteNoneIncompatible(FIREFOX_DESKTOP_UA));
		assertTrue(!UserAgentUtil.isSameSiteNoneIncompatible(CHROME_DESKTOP_67_UA));
		assertTrue(!UserAgentUtil.isSameSiteNoneIncompatible(UC_BROWSER_ANDROID_13_1_UA));
		assertTrue(!UserAgentUtil.isSameSiteNoneIncompatible(MACOS_10_6_UA));
	}

	@Test
	public void testIsSiteNoneInCompatible() {
		assertTrue(UserAgentUtil.isSameSiteNoneIncompatible(CHROME_DESKTOP_51_UA));
		assertTrue(UserAgentUtil.isSameSiteNoneIncompatible(CHROME_DESKTOP_66_UA));
		assertTrue(UserAgentUtil.isSameSiteNoneIncompatible(MACOS_10_14_UA));
		assertTrue(UserAgentUtil.isSameSiteNoneIncompatible(UC_BROWSER_ANDROID_11_5_UA));
		assertTrue(UserAgentUtil.isSameSiteNoneIncompatible(SAFARI_12_UA));
	}
}
