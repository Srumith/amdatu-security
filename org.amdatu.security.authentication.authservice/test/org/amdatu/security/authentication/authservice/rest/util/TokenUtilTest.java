/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest.util;

import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.amdatu.security.tokenprovider.TokenConstants.EXPIRATION_TIME;
import static org.amdatu.security.tokenprovider.TokenConstants.ISSUED_AT;
import static org.amdatu.security.tokenprovider.TokenConstants.NOT_BEFORE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.amdatu.security.authentication.authservice.rest.util.TokenUtil.RenewalResult;
import org.junit.Test;

/**
 * Test cases for {@link TokenUtil}.
 */
public class TokenUtilTest {
    private static final int MAX_TOKEN_LIFETIME = 180; // seconds

    @Test
    public void testDoNotRenewValidToken() throws Exception {
        Instant iat = now();
        Instant nbf = iat;
        Instant exp = nbf.plus(1, MINUTES);

        Map<String, String> tokenProps = createTokenProps(iat, nbf, exp);

        assertEquals(RenewalResult.STILL_VALID, extendTokenLifetime(tokenProps));
        // Show be exactly the same...
        assertTokenProps(tokenProps, iat, nbf, exp);
    }

    @Test
    public void testRenewExpiredToken() throws Exception {
        Instant iat = now().minus(1, MINUTES);
        Instant nbf = iat.plus(10, SECONDS);
        Instant exp = iat.plus(30, SECONDS);

        Map<String, String> tokenProps = createTokenProps(iat, nbf, exp);

        assertEquals(RenewalResult.RENEWED, extendTokenLifetime(tokenProps));
        // The nbf should be set to exp and exp should be null!
        assertTokenProps(tokenProps, iat, exp, null);
    }

    @Test
    public void testDoNotRenewExpiredTokenAfterMaxLifetime() throws Exception {
        Instant iat = now().minus(4, MINUTES);
        Instant nbf = iat;
        Instant exp = nbf.plus(1, MINUTES);

        Map<String, String> tokenProps = createTokenProps(iat, nbf, exp);

        // Since the iat is way before MAX_TOKEN_LIFETIME, we cannot extend the lifetime...
        assertEquals(RenewalResult.MAX_LIFETIME_REACHED, extendTokenLifetime(tokenProps));
    }

    @Test
    public void testSplitToken() {
        // Given
        String token256a  = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque orci neque, finibus non b"
                + "landit sit amet, varius in nulla. Aenean nisl quam, vestibulum ac purus sed, malesuada pellentesque "
                + "velit. Mauris ligula nulla, pharetra in egestas in, ultrices ameta";
        String token256b  = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque orci neque, finibus non b"
                + "landit sit amet, varius in nulla. Aenean nisl quam, vestibulum ac purus sed, malesuada pellentesque "
                + "velit. Mauris ligula nulla, pharetra in egestas in, ultrices ametb";
        String token256c  = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque orci neque, finibus non b"
                + "landit sit amet, varius in nulla. Aenean nisl quam, vestibulum ac purus sed, malesuada pellentesque "
                + "velit. Mauris ligula nulla, pharetra in egestas in, ultrices ametc";
        String token256d  = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque orci neque, finibus non b"
                + "landit sit amet, varius in nulla. Aenean nisl quam, vestibulum ac purus sed, malesuada pellentesque "
                + "velit. Mauris ligula nulla, pharetra in egestas in, ultrices ametd";
        String token1024 = token256a + token256b + token256c + token256d;
        String token3840 = token1024 + token1024 + token1024 + token256b + token256a + token256c;
        String token4096 = token3840 + token256b;
        String token16384 = token4096 + token3840 + token256b + token3840 + token256c + token3840 + token256a;

        testSplitToken(null, Collections.emptyList());
        testSplitToken(token256a, Collections.singletonList(token256a));
        testSplitToken(token3840, Collections.singletonList(token3840));
        testSplitToken(token3840 + "0", Arrays.asList(token3840, "0"));
        testSplitToken(token4096, Arrays.asList(token3840, token256b));
        testSplitToken(token16384, Arrays.asList(
                token3840,
                token256b + token1024 + token1024 + token1024 + token256b + token256a,
                token256c + token256b + token1024 + token1024 + token1024 + token256b,
                token256a + token256c + token256c + token1024 + token1024 + token1024,
                token256b + token256a + token256c + token256a
        ));
    }

    private void testSplitToken(String givenToken, List<String> expectedTokens) {
        // When
        List<String> chunkedTokens = TokenUtil.splitToken(givenToken);

        // Then
        assertEquals(expectedTokens, chunkedTokens);
    }

    private RenewalResult extendTokenLifetime(Map<String, String> tokenProps) {
        return extendTokenLifetime(tokenProps, now());
    }

    private RenewalResult extendTokenLifetime(Map<String, String> tokenProps, Instant now) {
        return TokenUtil.extendTokenLifetime(tokenProps, now, MAX_TOKEN_LIFETIME);
    }

    private static Map<String, String> createTokenProps(Instant iat, Instant nbf, Instant exp) {
        Map<String, String> result = new HashMap<>();
        result.put(ISSUED_AT, Long.toString(iat.getEpochSecond()));
        result.put(NOT_BEFORE, Long.toString(nbf.getEpochSecond()));
        result.put(EXPIRATION_TIME, Long.toString(exp.getEpochSecond()));
        return result;
    }

    private static void assertTokenProps(Map<String, String> tokenProps, Instant iat, Instant nbf, Instant exp) {
        assertInstant(iat, tokenProps.get(ISSUED_AT));
        assertInstant(nbf, tokenProps.get(NOT_BEFORE));
        assertInstant(exp, tokenProps.get(EXPIRATION_TIME));
    }

    private static void assertInstant(Instant expected, String actual) {
        if (expected == null) {
            assertNull(actual);
        }
        else {
            assertEquals(Long.toString(expected.getEpochSecond()), actual);
        }
    }
}
