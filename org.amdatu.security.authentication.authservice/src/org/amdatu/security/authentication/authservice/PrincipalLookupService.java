/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice;

import java.util.Map;
import java.util.Optional;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Provides a service that maps a set of credentials, obtained from a identity provider, to a
 * local principal.
 * <p>
 * This service is used to make a connection between users from a (remote) identity provider and
 * users that are locally known. As such, each realm has one (and only one) principal lookup
 * service. One could share a single principal lookup service amongst several realms.
 * <p>
 * One should use service properties for the principal lookup service registration to allow
 * it to be bound to a particular realm. Each realm defines a "<tt>principalLookupSelect</tt>"
 * configuration property that can be used to specify which principal lookup service should be
 * used for a particular realm.
 */
@ConsumerType
public interface PrincipalLookupService {

    /**
     * Maps the given credentials to a identifier that represents a principal in the local
     * application.
     *
     * @param credentials the credentials that should provide enough information to identify
     *        a principal in the system, cannot be <code>null</code> and should not be empty.
     * @return a principal identifier, if the user was found.
     */
    // tag::getPrincipal[]
    Optional<String> getPrincipal(Map<String, String> credentials);
    // end::getPrincipal[]

}
