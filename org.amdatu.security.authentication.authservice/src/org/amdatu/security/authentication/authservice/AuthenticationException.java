/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice;

/**
 * Denotes a failure in the authentication process.
 */
public class AuthenticationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final String m_code;

    /**
     * Creates a new {@link AuthenticationException} instance.
     */
    public AuthenticationException(String code) {
        super();
        m_code = code;
    }

    /**
     * Creates a new {@link AuthenticationException} instance.
     */
    public AuthenticationException(String code, String msg) {
        super(msg);
        m_code = code;
    }

    /**
     * Creates a new {@link AuthenticationException} instance.
     */
    public AuthenticationException(String code, String msg, Throwable cause) {
        super(msg, cause);
        m_code = code;
    }

    /**
     * @return the failure code that indicates why the authentication failed.
     */
    public String getCode() {
        return m_code;
    }
}
