/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest;

import static org.amdatu.security.authentication.authservice.rest.util.ConfigUtil.getBoolean;
import static org.amdatu.security.authentication.authservice.rest.util.ConfigUtil.getInteger;
import static org.amdatu.security.authentication.authservice.rest.util.ConfigUtil.getLong;
import static org.amdatu.security.authentication.authservice.rest.util.ConfigUtil.getMandatoryString;
import static org.amdatu.security.authentication.authservice.rest.util.ConfigUtil.getString;
import static org.amdatu.security.authentication.authservice.rest.util.ConfigUtil.getStringPlus;
import static org.amdatu.security.authentication.authservice.rest.util.ConfigUtil.getURI;
import static org.amdatu.security.authentication.authservice.rest.util.ConfigUtil.isValidSymbolicName;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_TARGET;

import java.net.URI;
import java.time.Duration;
import java.util.Dictionary;
import java.util.List;
import java.util.Optional;

import org.amdatu.security.authentication.authservice.AuthenticationInterceptor;
import org.osgi.service.cm.ConfigurationException;

/**
 * Provides the configuration for {@link AuthenticationResource}.
 */
public class AuthenticationRealmConfig {
    /**
     * REQUIRED. The name of this realm. Should be a valid BSN.
     */
    public static final String KEY_REALM = "realm";

    public static final String KEY_LANDING_PAGE_URL = "landingPageURL";
    public static final String KEY_LOGIN_URL = "loginURL";
    public static final String KEY_FAILURE_URL = "failureURL";
    /**
     * OPTIONAL. The name to use in the authentication cookie. Defaults to the name of the realm.
     */
    public static final String KEY_COOKIE_NAME = "cookieName";
    /**
     * OPTIONAL. The domain of the authentication cookie. Defaults to no domain.
     */
    public static final String KEY_COOKIE_DOMAIN = "cookieDomain";
    /**
     * OPTIONAL. The path of the authentication cookie. Defaults to '/'.
     */
    public static final String KEY_COOKIE_PATH = "cookiePath";
    /**
     * OPTIONAL. The maximum age of the authentication cookie. Defaults to '-1'.
     */
    public static final String KEY_COOKIE_MAX_AGE = "cookieMaxAge";
    /**
     * OPTIONAL. The maximum total of cookies to use for one authentication token. Defaults to '1'.
     */
    public static final String KEY_MAX_COOKIES = "maxCookies";
    /**
     * OPTIONAL. Defines where to land on after a redirect. If not given, it defaults to the loginurl
     */
    public static final String KEY_LOGOUT_REDIRECT = "logoutRedirectURL";
    /**
     * OPTIONAL. Defines whether or not the "end session" endpoint of the (remote) identity provider should be called when the user logs out. Defaults to <code>true</code>.
     */
    public static final String KEY_END_SESSION_AT_LOGOUT = "endSessionAtLogout";
    /**
     * OPTIONAL. Defines whether or not expired token will be auto-renewed in case the user is still logged in.
     */
    public static final String KEY_AUTO_RENEW_TOKEN = "autoRenewToken";
    /**
     * OPTIONAL. Defines what the maximum lifetime (in seconds) of a token can be in case tokens are auto renewed. The maximum lifetime is determined on the issuer time of the token.
     */
    public static final String KEY_TOKEN_MAX_LIFETIME = "tokenMaxLifetime";
    /**
     * OPTIONAL. Defines which IdProviders should be used as providers for user identity. Defaults to 'null', meaning all configured identity providers are used.
     */
    public static final String KEY_ID_PROVIDER_FILTER = "idProviderFilter";
    /**
     * OPTIONAL. Defines which principal lookup service should be used. Defaults to 'null', meaning the highest ranked one is used.
     */
    /* TODO This filter defaulting to null makes that this could break when you add some component that comes with a
     * PrincipalLookupService implementation like for example IG does.
     * Should we introduce a realm select service property that can be added to a principal lookup service?
     * (similar to application select in the JAX-RS Whiteboard)
     */
    public static final String KEY_PRINCIPAL_LOOKUP_SERVICE_FILTER = "principalLookupServiceFilter";
    /**
     * OPTIONAL. Defines which authentication interceptors should be used. Defaults to 'null', meaning all registered ones are used.
     */
    public static final String KEY_AUTH_INTERCEPTOR_SELECT = "authInterceptorSelect";

    private static final String DEFAULT_REALM = "local.realm";
    private static final String DEFAULT_COOKIE_DOMAIN = null;
    private static final String DEFAULT_COOKIE_PATH = "/";
    private static final int DEFAULT_COOKIE_MAX_AGE = -1;
    private static final int DEFAULT_MAX_COOKIES = 1;
    private static final String DEFAULT_URI = "../index.html";
    private static final boolean DEFAULT_END_SESSION_AT_LOGOUT = true;
    private static final boolean DEFAULT_AUTO_RENEW_TOKEN = false;
    private static final long DEFAULT_TOKEN_MAX_LIFETIME = Duration.ofDays(7).getSeconds();
    private static final String DEFAULT_CTX_PATH = "/auth";

    private final String m_realm;
    private final String m_contextPath;
    private final String m_httpWhiteboardTarget;
    private final URI m_landingPageURI;
    private final URI m_loginURI;
    private final URI m_failureURI;
    private final URI m_logoutRedirectURI;
    private final String m_cookieName;
    private final String m_cookieDomain;
    private final String m_cookiePath;
    private final int m_cookieMaxAge;
    private final int m_maxCookies;
    private final boolean m_endSessionAtLogout;
    private final boolean m_autoRenewToken;
    private final long m_tokenMaxLifetime;
    private final String m_idProviderFilter;
    private final String m_principalLookupServiceFilter;
    private final List<String> m_authInterceptorSelectFilter;

    /**
     * Creates a new {@link AuthenticationRealmConfig} instance.
     */
    public AuthenticationRealmConfig() {
        m_realm = DEFAULT_REALM;
        m_landingPageURI = m_loginURI = m_failureURI = URI.create(DEFAULT_URI);
        m_cookieName = m_realm;
        m_cookieDomain = DEFAULT_COOKIE_DOMAIN;
        m_cookiePath = DEFAULT_COOKIE_PATH;
        m_cookieMaxAge = DEFAULT_COOKIE_MAX_AGE;
        m_maxCookies = DEFAULT_MAX_COOKIES;
        m_endSessionAtLogout = DEFAULT_END_SESSION_AT_LOGOUT;
        m_autoRenewToken = DEFAULT_AUTO_RENEW_TOKEN;
        m_tokenMaxLifetime = DEFAULT_TOKEN_MAX_LIFETIME;
        m_logoutRedirectURI = m_loginURI;
        m_contextPath = DEFAULT_CTX_PATH;
        m_idProviderFilter = null;
        m_principalLookupServiceFilter = null;
        m_authInterceptorSelectFilter = null;
        m_httpWhiteboardTarget = null;
    }

    /**
     * Creates a new {@link AuthenticationRealmConfig} instance.
     */
    public AuthenticationRealmConfig(Dictionary<String, ?> config) throws ConfigurationException {
        m_realm = getMandatoryString(config, KEY_REALM);

        if (!isValidSymbolicName(m_realm)) {
            throw new ConfigurationException(KEY_REALM, "invalid value, should be a valid BSN!");
        }

        m_contextPath = getMandatoryString(config, HTTP_WHITEBOARD_CONTEXT_PATH);
        m_httpWhiteboardTarget = getString(config, HTTP_WHITEBOARD_TARGET, null);

        m_loginURI = getURI(config, KEY_LOGIN_URL, DEFAULT_URI);
        m_failureURI = getURI(config, KEY_FAILURE_URL, DEFAULT_URI);
        m_landingPageURI = getURI(config, KEY_LANDING_PAGE_URL, DEFAULT_URI);

        m_cookieName = getString(config, KEY_COOKIE_NAME, m_realm);
        m_cookiePath = getString(config, KEY_COOKIE_PATH, DEFAULT_COOKIE_PATH);
        m_cookieDomain = getString(config, KEY_COOKIE_DOMAIN, DEFAULT_COOKIE_DOMAIN);
        m_cookieMaxAge = getInteger(config, KEY_COOKIE_MAX_AGE, DEFAULT_COOKIE_MAX_AGE);
        m_maxCookies = getInteger(config, KEY_MAX_COOKIES, DEFAULT_MAX_COOKIES);

        m_endSessionAtLogout = getBoolean(config, KEY_END_SESSION_AT_LOGOUT, DEFAULT_END_SESSION_AT_LOGOUT);
        m_autoRenewToken = getBoolean(config, KEY_AUTO_RENEW_TOKEN, DEFAULT_AUTO_RENEW_TOKEN);
        m_tokenMaxLifetime = getLong(config, KEY_TOKEN_MAX_LIFETIME, DEFAULT_TOKEN_MAX_LIFETIME);
        m_logoutRedirectURI = getURI(config, KEY_LOGOUT_REDIRECT, m_loginURI.toString());

        m_idProviderFilter = getString(config, KEY_ID_PROVIDER_FILTER, null);
        m_principalLookupServiceFilter = getString(config, KEY_PRINCIPAL_LOOKUP_SERVICE_FILTER, null);
        m_authInterceptorSelectFilter = getStringPlus(config, KEY_AUTH_INTERCEPTOR_SELECT);
    }

    /**
     * @return the LDAP-style filter used to select which {@link AuthenticationInterceptor}s to use, can be <code>null</code>.
     */
    public List<String> getAuthenticationInterceptorSelectFilter() {
        return m_authInterceptorSelectFilter;
    }

    public String getContextName() {
        return "amdatu.security.authentication." + m_realm;
    }

    /**
     * @return the context path, cannot be <code>null</code>.
     */
    public String getContextPath() {
        return m_contextPath;
    }

    /**
     * @return the domain of the cookie to return, cannot be <code>null</code>.
     */
    public Optional<String> getCookieDomain() {
        return Optional.ofNullable(m_cookieDomain);
    }

    /**
     * @return the maximum age of returned cookies, in seconds. If &lt;= 0, the cookie will follow the lifetime of the identity token (if present).
     */
    public int getCookieMaxAge() {
        return m_cookieMaxAge;
    }

    /**
     * @return the maximum total of cookies to use. If &gt; 1, the token may be chunked into multiple cookies depending on the token size (if size &gt; 3840 bytes).
     */
    public int getMaxCookies() {
        return m_maxCookies;
    }

    /**
     * @return the name of the cookie to return, never <code>null</code>.
     */
    public String getCookieName() {
        return m_cookieName;
    }

    /**
     * @return the path of the cookie to return, never <code>null</code>.
     */
    public String getCookiePath() {
        return m_cookiePath;
    }

    /**
     * @return the URI to redirect to after an authentication failure.
     */
    public URI getFailureURL() {
        return m_failureURI;
    }

    /**
     * @return the LDAP-style filter used to select which HTTP service to use, can be <code>null</code>
     *
     * @see org.osgi.service.http.whiteboard.HttpWhiteboardConstants#HTTP_WHITEBOARD_TARGET
     */
    public String getHttpWhiteboardTarget() {
        return m_httpWhiteboardTarget;
    }

    /**
     * @return the LDAP-style filter selecting the identity providers, can be <code>null</code>.
     */
    public String getIdProviderFilter() {
        return m_idProviderFilter;
    }

    /**
     * @return the URI to redirect to after a successful authentication.
     */
    public URI getLandingPageURL() {
        return m_landingPageURI;
    }

    /**
     * @return the URI to redirect to in case a login is necessary.
     */
    public URI getLoginURL() {
        return m_loginURI;
    }

    /**
     * @return the URI to redirect to in case a logout is done.
     */
    public URI getLogoutRedirectURL() {
        return m_logoutRedirectURI;
    }

    /**
     * @return the LDAP-style filter used to select a principal lookup service, can be <code>null</code>.
     */
    public String getPrincipalLookupServiceFilter() {
        return m_principalLookupServiceFilter;
    }

    /**
     * @return the name of the realm, cannot be <code>null</code>.
     */
    public String getRealm() {
        return m_realm;
    }

    /**
     * @return the maximum lifetime allowed for a token that is auto renewed, -1 of no maximum is defined.
     */
    public long getTokenMaxLifetime() {
        return m_tokenMaxLifetime;
    }

    /**
     * @return <code>true</code> if expired token should be automatically be renewed in case the session still is active, <code>false</code> (the default) to automatically end the session when a token expires.
     */
    public boolean isAutoRenewToken() {
        return m_autoRenewToken;
    }

    /**
     * @return <code>true</code> if the "end-session" endpoint of the (remote) identity provider should be called upon logout, <code>false</code> if it should not be called.
     */
    public boolean isEndSessionAtLogout() {
        return m_endSessionAtLogout;
    }
}
