/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserAgentUtil {

	private UserAgentUtil() {

	}

	public static boolean isSameSiteNoneIncompatible(String useragent) {
		return hasWebKitSameSiteBug(useragent) || dropsUnrecognizedSameSiteCookies(useragent);
	}

	private static boolean hasWebKitSameSiteBug(String useragent) {
		return isIosVersion(12, useragent) || isMacEmbeddedBrowser(useragent)
				|| (isMacosxVersion(10, 14, useragent) && (isSafari(useragent)));
	}

	private static boolean isIosVersion(int major, String useragent) {
		Pattern pattern = Pattern.compile("\\(iP.+; CPU .*OS (\\d+)[_\\d]*.*\\) AppleWebKit\\/");
		Matcher matcher = pattern.matcher(useragent);
		if (matcher.find()) {
			return matcher.group(1).equals(String.valueOf(major));
		}
		return false;
	}

	private static boolean isMacosxVersion(int major, int minor, String useragent) {
		Pattern pattern = Pattern.compile("\\(Macintosh;.*Mac OS X (\\d+)_(\\d+)[_\\d]*.*\\) AppleWebKit\\/");
		Matcher matcher = pattern.matcher(useragent);
		if (matcher.find()) {
			String majorFound = matcher.group(1);
			String minorFound = matcher.group(2);
			return majorFound.equals(String.valueOf(major)) && minorFound.equals(String.valueOf(minor));
		}
		return false;
	}

	private static boolean isSafari(String useragent) {
		Pattern pattern = Pattern.compile("Version\\/.* Safari\\/");
		Matcher matcher = pattern.matcher(useragent);
		return matcher.find() && !isChromiumBased(useragent);
	}

	private static boolean isChromiumBased(String useragent) {
		Pattern pattern = Pattern.compile("Chrom(e|ium)");
		Matcher matcher = pattern.matcher(useragent);
		return matcher.find();
	}

	private static boolean isMacEmbeddedBrowser(String useragent) {
		Pattern pattern = Pattern.compile(
				"^Mozilla\\/[\\.\\d]+ \\(Macintosh;.*Mac OS X [_\\d]+\\) AppleWebKit\\/[\\.\\d]+ \\(KHTML, like Gecko\\)$");
		Matcher matcher = pattern.matcher(useragent);
		return matcher.find();
	}

	private static boolean isChromiumVersionAtLeast(int major, String useragent) {
		Pattern pattern = Pattern.compile("Chrom[^ \\/]+\\/(\\d+)[\\.\\d]");
		Matcher matcher = pattern.matcher(useragent);
		if (matcher.find()) {
			int version = Integer.parseInt(matcher.group(1));
			return version >= major;
		}
		return false;
	}

	private static boolean isUcBrowser(String useragent) {
		Pattern pattern = Pattern.compile("UCBrowser\\/");
		Matcher matcher = pattern.matcher(useragent);
		return matcher.find();
	}

	private static boolean dropsUnrecognizedSameSiteCookies(String useragent) {
		if (isUcBrowser(useragent)) {
			return !isUcBrowserVersionAtLeast(12, 13, 2, useragent);
		}
		return isChromiumBased(useragent) && isChromiumVersionAtLeast(51, useragent)
				&& !isChromiumVersionAtLeast(67, useragent);
	}

	private static boolean isUcBrowserVersionAtLeast(int major, int minor, int build, String useragent) {
		Pattern pattern = Pattern.compile("UCBrowser\\/(\\d+)\\.(\\d+)\\.(\\d+)[\\.\\d]* ");
		Matcher matcher = pattern.matcher(useragent);
		if (matcher.find()) {
			int majorFound = Integer.parseInt(matcher.group(1));
			int minorFound = Integer.parseInt(matcher.group(2));
			int buildFound = Integer.parseInt(matcher.group(3));
			if (majorFound != major) {
				return majorFound > major;
			}
			if (minorFound != minor) {
				return minorFound > minor;
			}
			return buildFound >= build;
		}
		return false;
	}
}
