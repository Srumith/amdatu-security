/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest;

import static java.lang.Boolean.FALSE;
import static java.util.Collections.singletonMap;
import static java.util.Comparator.reverseOrder;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.seeOther;
import static javax.ws.rs.core.Response.status;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.amdatu.security.authentication.authservice.AuthenticationConstants.REQ_ERROR_CODE;
import static org.amdatu.security.authentication.authservice.AuthenticationConstants.REQ_ID_TOKEN;
import static org.amdatu.security.authentication.authservice.AuthenticationConstants.REQ_LOCAL_ID;
import static org.amdatu.security.authentication.authservice.AuthenticationConstants.REQ_PROVIDER_NAME;
import static org.amdatu.security.authentication.authservice.AuthenticationConstants.REQ_PROVIDER_TYPE;
import static org.amdatu.security.authentication.authservice.AuthenticationInterceptor.LoginReason.TOKEN_REFRESHED;
import static org.amdatu.security.authentication.authservice.AuthenticationInterceptor.LogoutReason.LOGIN_REQUIRED;
import static org.amdatu.security.authentication.authservice.AuthenticationInterceptor.RejectReason.INVALID_REQUEST;
import static org.amdatu.security.authentication.authservice.AuthenticationInterceptor.RejectReason.PROVIDER_ERROR;
import static org.amdatu.security.authentication.authservice.AuthenticationInterceptor.RejectReason.UNKNOWN_OR_INVALID_USER;
import static org.amdatu.security.authentication.authservice.rest.AuthenticationTokenCookieInterceptor.ATTR_INVALID_TOKEN_REASON;
import static org.amdatu.security.authentication.authservice.rest.AuthenticationTokenCookieInterceptor.ATTR_TOKEN_PROPERTIES;
import static org.amdatu.security.authentication.authservice.rest.util.HttpUtil.isXmlHttpRequest;
import static org.amdatu.security.authentication.authservice.rest.util.TokenUtil.extendTokenLifetime;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_INTERACTION_REQUIRED;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_INVALID_REQUEST;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_LOGGED_OUT;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_LOGIN_REQUIRED;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_UNAUTHORIZED_CLIENT;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.ERROR_CODE;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.ID_TOKEN;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.LOCAL_ID;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROMPT;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROMPT_LOGIN;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_NAME;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_TYPE;
import static org.amdatu.security.tokenprovider.TokenConstants.SUBJECT;
import static org.osgi.service.http.context.ServletContextHelper.REMOTE_USER;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_ERROR;
import static org.osgi.service.log.LogService.LOG_INFO;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.amdatu.security.authentication.authservice.AuthenticationContext;
import org.amdatu.security.authentication.authservice.AuthenticationException;
import org.amdatu.security.authentication.authservice.AuthenticationHandler;
import org.amdatu.security.authentication.authservice.AuthenticationInterceptor;
import org.amdatu.security.authentication.authservice.AuthenticationInterceptor.LoginReason;
import org.amdatu.security.authentication.authservice.AuthenticationInterceptor.LogoutReason;
import org.amdatu.security.authentication.authservice.AuthenticationInterceptor.RejectReason;
import org.amdatu.security.authentication.authservice.PrincipalLookupService;
import org.amdatu.security.authentication.authservice.RejectInfo;
import org.amdatu.security.authentication.authservice.rest.util.TokenUtil.RenewalResult;
import org.amdatu.security.authentication.idprovider.IdProvider;
import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.InvalidTokenException.Reason;
import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.util.URIBuilder;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * Provides a front-end REST resource that allows you to login and logout from a system.
 */
@Path("/")
public class AuthenticationResource implements AuthenticationHandler {

    static class AuthContextImpl implements AuthenticationContext {
        private final String m_providerType;
        private final Map<String, String> m_userIdentity;
        private final HttpServletRequest m_req;
        private final HttpServletResponse m_resp;

        AuthContextImpl(String providerType, String providerName, Map<String, String> userIdentity,
            HttpServletRequest req, HttpServletResponse resp) {
            m_providerType = providerType;
            m_userIdentity = userIdentity;
            m_resp = resp;
            m_req = req;

            // Provide some useful information for our callers downstream...
            m_req.setAttribute(REQ_PROVIDER_TYPE, providerType);
            m_req.setAttribute(REQ_PROVIDER_NAME, providerName);
            m_req.setAttribute(REMOTE_USER, userIdentity.get(SUBJECT));
            Optional.ofNullable(userIdentity.get(ERROR_CODE))
                .ifPresent(error -> m_req.setAttribute(REQ_ERROR_CODE, error));
        }

        AuthContextImpl(TokenProperties tokenProps, HttpServletRequest req, HttpServletResponse resp) {
            m_providerType = tokenProps.getProviderType().orElse(null);
            m_userIdentity = tokenProps.m_userIdentity;
            m_resp = resp;
            m_req = req;

            // Provide some useful information for our callers downstream...
            tokenProps.getProviderType()
                .ifPresent(providerType -> m_req.setAttribute(REQ_PROVIDER_TYPE, providerType));
            tokenProps.getProviderName()
                .ifPresent(providerName -> m_req.setAttribute(REQ_PROVIDER_NAME, providerName));
            Optional.ofNullable(tokenProps.m_userIdentity.get(SUBJECT))
                .ifPresent(subject -> m_req.setAttribute(REMOTE_USER, subject));
            Optional.ofNullable(tokenProps.m_userIdentity.get(ERROR_CODE))
                .ifPresent(error -> m_req.setAttribute(REQ_ERROR_CODE, error));
        }

        @Override
        public Optional<String> getOptionalProviderType() {
            return Optional.ofNullable(m_providerType);
        }

        @Override
        public HttpServletRequest getServletRequest() {
            return m_req;
        }

        @Override
        public HttpServletResponse getServletResponse() {
            return m_resp;
        }

        @Override
        public Map<String, String> getUserIdentity() {
            return m_userIdentity;
        }

    }

    static class TokenProperties {
        final Optional<Reason> m_reason;
        final Map<String, String> m_userIdentity;

        public TokenProperties(Map<String, String> userIdentity, Reason reason) {
            m_userIdentity = userIdentity == null ? new HashMap<>() : userIdentity;
            m_reason = Optional.ofNullable(reason);
        }

        /**
         * @return the type of the provider (as included in the user identity).
         */
        Optional<String> getProviderType() {
            return Optional.ofNullable(m_userIdentity.get(PROVIDER_TYPE));
        }

        /**
         * @return the name of the provider (as included in the user identity).
         */
        Optional<String> getProviderName() {
            return Optional.ofNullable(m_userIdentity.get(PROVIDER_NAME));
        }

        /**
         * @return <code>true</code> iff the token was expired.
         */
        boolean isTokenExpired() {
            return m_reason.map(Reason.TOKEN_EXPIRED::equals).orElse(Boolean.FALSE);
        }

        /**
         * @return <code>true</code> iff the token was valid and contains attributes that identify the user.
         */
        boolean isValid() {
            return !m_reason.isPresent() && !m_userIdentity.isEmpty();
        }
    }

    private final AuthenticationRealmConfig m_config;
    // Injected by Felix DM...
    // TODO having this injected as a field makes it hard to debug issues where multiple PrincipalLookupService instances intended for different realms match the filter
    private volatile PrincipalLookupService m_principalLookupService;
    private volatile LogService m_log;

    private final Map<ServiceReference<AuthenticationInterceptor>, AuthenticationInterceptor> m_authenticationInterceptors;
    private final Map<String, IdProvider> m_idProviders;

    /**
     * Creates a new {@link AuthenticationResource} instance.
     */
    public AuthenticationResource(AuthenticationRealmConfig config) {
        m_config = config;
        m_idProviders = new ConcurrentHashMap<>();
        m_authenticationInterceptors = new TreeMap<>(reverseOrder());
    }

    @POST
    @Path("/login/{provider}")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_FORM_URLENCODED)
    @Description("Called by the identity provider (= local or remote) to return the result of the authentication process.")
    public Response finishLoginFormPost(@PathParam("provider") String providerName,
        @Context HttpServletRequest req, @Context HttpServletResponse resp) {
        return finishLogin(providerName, req, resp);
    }

    @GET
    @Path("/login/{provider}")
    @Produces(APPLICATION_JSON)
    @Description("Called by the identity provider (= local or remote) to return the result of the authentication process.")
    public Response finishLoginQuery(@PathParam("provider") String providerName,
        @Context HttpServletRequest req, @Context HttpServletResponse resp) {
        return finishLogin(providerName, req, resp);
    }

    @GET
    @Path("/login/providers")
    @Produces(APPLICATION_JSON)
    @Description("Returns the names of the available identity providers that can be used to authenticate users.")
    public Response getIdProviders(@QueryParam("verbose") @DefaultValue("false") boolean verbose) {
        Map<String, IdProvider> idProviders = new HashMap<>(m_idProviders);

        if (!verbose) {
            return Response.ok(idProviders.keySet()).build();
        }

        List<Map<String, String>> results = new ArrayList<>();
        for (Entry<String, IdProvider> entry : idProviders.entrySet()) {
            Map<String, String> info = new HashMap<>();
            info.put("name", entry.getKey());
            info.put("type", entry.getValue().getType());
            results.add(info);
        }

        return Response.ok(results).build();
    }

    /**
     * This method is used by all users of {@link AuthenticationHandler} and is implemented here as we need access
     * to the configured failure page and cookie name.
     */
    @Override
    public Optional<RejectInfo> handleSecurity(HttpServletRequest request, HttpServletResponse response) {
        // Initiate the authentication process...
        onAuthenticationStart(request, response);

        try {
            TokenProperties tokenProps = getTokenProperties(request);
            AuthenticationContext authCtx = new AuthContextImpl(tokenProps, request, response);

            if (!isAuthenticated(m_config, tokenProps)) {
                // Token no longer valid and we're not allowed to auto-renew it...
                log(LOG_DEBUG, authCtx, "Token not valid. Ending session to force new login!");

                // Call out to all interceptors...
                onLogout(authCtx, LOGIN_REQUIRED);

                // Give the caller a clue what is required of him...
                return RejectInfo.create(CODE_LOGIN_REQUIRED, UNAUTHORIZED.getStatusCode(), m_config.getFailureURL());
            }

            // User was logged in, and its token was still valid or to be renewed...

            Map<String, String> userIdentity = tokenProps.m_userIdentity;

            // In case of local login put the account id on the request.
            Optional.ofNullable(userIdentity.get(LOCAL_ID))
                .ifPresent(localId -> request.setAttribute(REQ_LOCAL_ID, localId));

            // In case of OpenIDConnect login put the id token on the request.
            Optional.ofNullable(userIdentity.get(ID_TOKEN))
                .ifPresent(idToken -> request.setAttribute(REQ_ID_TOKEN, idToken));

            if (tokenProps.isTokenExpired()) {
                // if we get here the token can be extended, else it should have been
                // rejected. Update the token cookie, to prolong the users' session...
                onLogin(authCtx, TOKEN_REFRESHED);
            }

            return RejectInfo.none();
        }
        finally {
            onAuthenticationEnd(request, response);
        }
    }

    @POST
    @Path("/login")
    @Consumes(APPLICATION_FORM_URLENCODED)
    @Description("Called by the client/user agent to initiate the authentication with the identity provider.")
    public Response initiateLogin(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws UnsupportedEncodingException {
        onAuthenticationStart(req, resp);

        try {
            TokenProperties tokenProps = getTokenProperties(req);

            // We want any other form-parameters to retain as-is, hence we do not use the
            // FormParam annotation of JAXRS as it mangles our request parameters!
            // Use the same provider as the token indicates, this way, we'll always
            // will use the right provider to refresh our token...
            String providerName = tokenProps.getProviderName()
                .orElse(req.getParameter(PROVIDER_NAME));

            if (providerName == null) {
                log(LOG_ERROR,
                        "Invalid login request, no providerName found in token properties and request has no '%1$s' parameter. " +
                                "If this is a request from a custom login page make sure the '%1$s' parameter is sent.", PROVIDER_NAME);
                return respondFailure(m_config, req, CODE_INVALID_REQUEST);
            }

            Optional<IdProvider> provider = getProvider(providerName);

            if (!provider.isPresent()) {
                log(LOG_ERROR,
                        "Failed to process login request, no provider available with name '%s'", providerName);
                return respondFailure(m_config, req, CODE_INVALID_REQUEST);
            }

            AuthenticationContext authCtx = new AuthContextImpl(tokenProps, req, resp);

            if (tokenProps.isValid()) {
                try {
                    // Try to refresh the token, and if refreshed, update the cookie for the user...
                    provider
                        .ifPresent(idp -> {
                            log(LOG_DEBUG, authCtx, "Trying to refresh token...");

                            if (idp.refreshToken(tokenProps.m_userIdentity)) {
                                log(LOG_DEBUG, authCtx, "Token refreshed.");

                                // Notify our interceptors...
                                onLogin(authCtx, TOKEN_REFRESHED);
                            }
                            else {
                                log(LOG_DEBUG, authCtx, "Token NOT refreshed.");
                            }
                        });

                    // In case we're already logged in (and our token is apparently still valid),
                    // we can go straight through our final destination...
                    return respondSuccess(req, m_config.getLandingPageURL()).build();
                }
                catch (InvalidTokenException e) {
                    log(LOG_DEBUG, authCtx, e, "Token refresh failed. Forcing new login.");
                    // Fall through, and try to restart the whole login sequence...
                }
            }

            log(LOG_DEBUG, authCtx, "Initiating new login...");

            // We want to get called back on this endpoint, but with a GET method instead
            // and with the provider appended. Also, ensure that the callback URI does not
            // include any query string or fragment (see AMDATUSEC-31)...
            URI callbackURI = URIBuilder.of(req)
                .appendToPath(URLEncoder.encode(providerName, "UTF-8"))
                .clearFragment()
                .clearQuery()
                .build();

            // See if there is a identity provider supplied as parameter, if so, get its authorization URI, otherwise return a failure...
            return provider
                .map(idp -> idp.getAuthenticationURI(callbackURI, req.getParameterMap()))
                .map(uri -> seeOther(uri).build())
                .orElseGet(() -> {
                    onLoginRejected(authCtx, INVALID_REQUEST);

                    return respondFailure(m_config, req, CODE_INVALID_REQUEST);
                });
        }
        finally {
            onAuthenticationEnd(req, resp);
        }
    }

    @GET
    @Path("/login")
    @Produces(APPLICATION_JSON)
    @Description("Checks whether the request contains a valid token indicating that the user is properly authenticated.")
    public Response isLoggedIn(@Context HttpServletRequest req, @Context HttpServletResponse resp) {
        onAuthenticationStart(req, resp);

        try {
            TokenProperties props = getTokenProperties(req);

            if (!isAuthenticated(m_config, props)) {
                // No or invalid token, not authenticated...
                return status(FORBIDDEN).build();
            }

            return status(OK).entity(props.m_userIdentity).build();
        }
        finally {
            onAuthenticationEnd(req, resp);
        }
    }

    @GET
    @Path("/logout")
    @Produces(APPLICATION_JSON)
    public Response linkLogout(@Context HttpServletRequest req, @Context HttpServletResponse resp) {
        return seeOther(logout(req, resp, null)).build();
    }

    @POST
    @Path("/logout")
    @Produces(APPLICATION_JSON)
    @Description("Called by the client/user agent to log out.")
    public Response logout(@Context HttpServletRequest req, @Context HttpServletResponse resp) {
        return seeOther(logout(req, resp, null)).build();
    }

    @GET
    @Path("/logout/{state}")
    @Produces(APPLICATION_JSON)
    @Description("Called by the client/user agent to log out with state for openidconnector.")
    public Response logoutOpenIdConnect(@Context HttpServletRequest req, @Context HttpServletResponse resp, @PathParam("state") String state) {
        return seeOther(logout(req, resp, state)).build();
    }

    @GET
    @Path("/logoutlocal/{state}")
    @Produces(APPLICATION_JSON)
    @Description("Called by the client/user agent to log out and return the url to end the session at openid endsessionendpoint.")
    public Response logoutLocal(@Context HttpServletRequest req, @Context HttpServletResponse resp,
            @PathParam("state") String state) {
        URI logoutURI = logout(req, resp, state);
        Map<String, String> map = new HashMap<>();
        map.put("LogoutRedirectURL", createURI(m_config.getLogoutRedirectURL(), CODE_LOGGED_OUT).toString());
        if (m_config.isEndSessionAtLogout()) {
            map.put("EndSessionURL", logoutURI.toString());
        }
        return Response.ok(map).build();
    }

    private URI logout(@Context HttpServletRequest req, @Context HttpServletResponse resp, String state) {
        onAuthenticationStart(req, resp);

        try {
            TokenProperties tokenProps = getTokenProperties(req);

            Map<String, String> userIdentity = tokenProps.m_userIdentity;
            AuthenticationContext authCtx = new AuthContextImpl(tokenProps, req, resp);

            String providerName = userIdentity.get(PROVIDER_NAME);
            String code = CODE_LOGGED_OUT;
            URI destURI = m_config.getLogoutRedirectURL();

            try {
                // Call out to all interceptors...
                onLogout(authCtx, LogoutReason.USER_REQUEST);

                if (m_config.isEndSessionAtLogout()) {
                    log(LOG_INFO, authCtx, "Ending session at ID provider...");

                    URI callbackURI = createURI(destURI, code);
                    destURI = getProvider(providerName)
                        .flatMap(p -> p.getEndSessionURI(callbackURI, userIdentity, state))
                        .orElse(callbackURI);
                } else {
                    destURI = createURI(destURI, code);
                }
            }
            catch (AuthenticationException e) {
                log(LOG_INFO, authCtx, e, "Log out failed!");
                destURI = createURI(destURI, e.getCode());
            }

            return destURI;
        }
        finally {
            onAuthenticationEnd(req, resp);
        }
    }

    /**
     * Called by Felix DM for all {@link AuthenticationInterceptor}s that are registered.
     */
    protected final void addAuthInterceptor(ServiceReference<AuthenticationInterceptor> srvRef,
            AuthenticationInterceptor srv) {
        m_authenticationInterceptors.put(srvRef, srv);
    }

    /**
     * Called by Felix DM for all {@link IdProvider}s that are registered.
     */
    protected final void addIdProvider(ServiceReference<IdProvider> srvRef, IdProvider srv) {
        String providerName = (String) srvRef.getProperty(PROVIDER_NAME);
        m_idProviders.put(providerName, srv);
    }

    /**
     * Called by Felix DM for all {@link AuthenticationInterceptor}s that are registered.
     */
    protected final void removeAuthInterceptor(ServiceReference<AuthenticationInterceptor> srvRef,
            AuthenticationInterceptor srv) {
        m_authenticationInterceptors.remove(srvRef, srv);
    }

    /**
     * Called by Felix DM for all {@link IdProvider}s that are registered.
     */
    protected final void removeIdProvider(ServiceReference<IdProvider> srvRef, IdProvider srv) {
        String providerName = (String) srvRef.getProperty(PROVIDER_NAME);
        m_idProviders.remove(providerName, srv);
    }

    private URI createURI(URI uri, String code) {
        URIBuilder builder = new URIBuilder(uri);
        if (code != null) {
            builder.appendToQuery("msg", code);
        }
        return builder.build();
    }

    private Response finishLogin(String providerName, HttpServletRequest req, HttpServletResponse resp) {
        // AMDATUSEC-31 ensure that the callback URI does not include any query string or fragment...
        URI callbackURI = URIBuilder.of(req)
            .clearFragment()
            .clearQuery()
            .build();

        Optional<IdProvider> idProvider = getProvider(providerName);

        String providerType = idProvider
            .map(IdProvider::getType)
            .orElse(null);

        if (providerType == null) {
            log(LOG_DEBUG, "No identity provider found with name '%s': login will be refused!", providerName);
        }

        // Determine the actual user identity using the IdProvider. Note that this
        // could imply a call to an external service...
        Map<String, String> userIdentity = idProvider
            .map(provider -> provider.getUserIdentity(callbackURI, req.getParameterMap()))
            .map(this::lookupPrincipal)
            .orElseGet(() -> singletonMap(ERROR_CODE, CODE_UNAUTHORIZED_CLIENT));

        String error = userIdentity.get(ERROR_CODE);
        AuthenticationContext authCtx = new AuthContextImpl(providerType, providerName, userIdentity, req, resp);

        if (CODE_INTERACTION_REQUIRED.equals(error) || CODE_LOGIN_REQUIRED.equals(error)) {
            // Force the user to re-authenticate itself...
            log(LOG_INFO, authCtx, "Forcing new login: ID provider needs explicity login!");

            // Try to enforce a login at the remote IdProvider...
            Map<String, String[]> params = new HashMap<>(req.getParameterMap());
            params.put(PROMPT, new String[] { PROMPT_LOGIN });

            return idProvider
                .map(idp -> idp.getAuthenticationURI(callbackURI, params))
                .map(uri -> seeOther(uri).build())
                .orElseGet(() -> {
                    onLoginRejected(authCtx, INVALID_REQUEST);

                    return respondFailure(m_config, req, error);
                });
        }
        else if (error != null) {
            log(LOG_INFO, authCtx, "Login failed: ID provider returned error code %s!", error);

            onLoginRejected(authCtx, PROVIDER_ERROR);

            return respondFailure(m_config, req, error);
        }

        try {
            // Call out to all interceptors...
            onLogin(authCtx, LoginReason.USER_REQUEST);

            log(LOG_DEBUG, authCtx, "Login successful!");

            return respondSuccess(req, m_config.getLandingPageURL()).build();
        }
        catch (AuthenticationException e) {
            log(LOG_INFO, authCtx, "User unknown or provided invalid credentials!");

            onLoginRejected(authCtx, UNKNOWN_OR_INVALID_USER);

            return respondFailure(m_config, req, e.getCode());
        }
    }

    private List<AuthenticationInterceptor> getAuthenticationInterceptors() {
        return new ArrayList<>(m_authenticationInterceptors.values());
    }

    private Optional<IdProvider> getProvider(String name) {
        return Optional.ofNullable(m_idProviders.get(name));
    }

    @SuppressWarnings("unchecked")
    private TokenProperties getTokenProperties(HttpServletRequest req) {
        Map<String, String> properties = (Map<String, String>) req.getAttribute(ATTR_TOKEN_PROPERTIES);
        Reason reason = (Reason) req.getAttribute(ATTR_INVALID_TOKEN_REASON);

        return new TokenProperties(properties, reason);
    }

    private boolean isAuthenticated(AuthenticationRealmConfig cfg, TokenProperties tokenProps) {
        if (tokenProps.isTokenExpired()) {
            if (!cfg.isAutoRenewToken() || !tokenLifetimeExtended(tokenProps, cfg.getTokenMaxLifetime())) {
                // the token is expired and could not be extended
                return false;
            }
        } else if (!tokenProps.isValid()) {
            // the token is invalid
            return false;
        }

        Map<String, String> userIdentity = tokenProps.m_userIdentity;

        // Verify that the given user identity is known to the rest of our application...
        return m_principalLookupService.getPrincipal(userIdentity)
            .map(localUserId -> Objects.equals(localUserId, userIdentity.get(SUBJECT)))
            .orElse(FALSE);
    }

    private void log(int level, AuthenticationContext ctx, String msg, Object... args) {
        log(level, ctx, null, msg, args);
    }

    private void log(int level, AuthenticationContext ctx, Throwable t, String msg, Object... args) {
        StringBuilder sb = new StringBuilder();

        sb.append(" [user identity: ").append(ctx.getUserIdentity().getOrDefault(SUBJECT, "<unknown>")).append("]");

        ctx.getOptionalProviderType()
            .ifPresent(provider -> sb.append(" [provider: ").append(provider).append("]"));

        HttpServletRequest req = ctx.getServletRequest();
        sb.append(" [request origin: ").append(req.getRemoteAddr()).append(":").append(req.getRemotePort()).append("]");
        sb.append(" [requested url: ").append(req.getRequestURL()).append("]");
        sb.append(" - ").append(String.format(msg, args));

        if (t == null) {
            m_log.log(level, sb.toString());
        }
        else {
            m_log.log(level, sb.toString(), t);
        }
    }

    private void log(int level, String msg, Object... args) {
        m_log.log(level, String.format(msg, args));
    }

    private Map<String, String> lookupPrincipal(Map<String, String> userIdentity) {
        // Only if there's no error code present, do the mapping to a local user...
        if (userIdentity.containsKey(ERROR_CODE)) {
            log(LOG_DEBUG, "Refusing to lookup principal for user due to error code '%s'",
                userIdentity.get(ERROR_CODE));
        }
        else {
            Optional<String> principal = m_principalLookupService.getPrincipal(userIdentity);

            if (principal.isPresent()) {
                // User is known, add the internal user identifier as subject...
                userIdentity.put(SUBJECT, principal.get());
            }
            else {
                log(LogService.LOG_INFO, "No principal for credentials: %s", userIdentity);
                userIdentity.put(ERROR_CODE, CODE_UNAUTHORIZED_CLIENT);
            }
        }
        return userIdentity;
    }

    private void onAuthenticationStart(HttpServletRequest request, HttpServletResponse response) {
        getAuthenticationInterceptors().forEach(interceptor -> {
            try {
                interceptor.onAuthenticationStart(request, response);
            }
            catch (Exception e) {
                log(LOG_WARNING, "Interceptor#onAuthenticationStart failure: %s", interceptor.getClass(), e);
            }
        });
    }

    private void onAuthenticationEnd(HttpServletRequest request, HttpServletResponse response) {
        getAuthenticationInterceptors().forEach(interceptor -> {
            try {
                interceptor.onAuthenticationEnd(request, response);
            }
            catch (Exception e) {
                log(LOG_WARNING, "Interceptor#onAuthenticationEnd failure: %s", interceptor.getClass(), e);
            }
        });
    }

    private void onLogin(AuthenticationContext context, LoginReason reason) throws AuthenticationException {
        // We allow the interceptors to veto the login...
        getAuthenticationInterceptors().forEach(interceptor -> interceptor.onLogin(context, reason));
    }

    private void onLoginRejected(AuthenticationContext context, RejectReason reason) throws AuthenticationException {
        getAuthenticationInterceptors().forEach(interceptor -> {
            try {
                interceptor.onLoginRejected(context, reason);
            }
            catch (Exception e) {
                log(LOG_WARNING, "Interceptor#onLoginRejected failure: %s", interceptor.getClass(), e);
            }
        });
    }

    private void onLogout(AuthenticationContext context, LogoutReason reason) throws AuthenticationException {
        // We allow the interceptors to veto the logout...
        getAuthenticationInterceptors().forEach(interceptor -> interceptor.onLogout(context, reason));
    }

    private Response respondFailure(AuthenticationRealmConfig cfg, HttpServletRequest req, String reason) {
        URIBuilder builder = new URIBuilder(cfg.getFailureURL());
        if (reason != null) {
            builder.appendToQuery("msg", reason);
        }
        URI destURI = builder.build();

        ResponseBuilder rb;
        if (isXmlHttpRequest(req) || destURI == null) {
            // Ajax request or no failure page: respond with a forbidden
            // and a plain body instead of redirecting to a failure page
            // (this reduced traffic a bit as the client probably isn't
            // interested in the redirected page but more in the result
            // of the operation)...
            rb = status(Status.FORBIDDEN).entity(reason);
        }
        else {
            rb = seeOther(destURI);
        }

        return rb.build();
    }

    private ResponseBuilder respondSuccess(HttpServletRequest req, URI destURI) {
        ResponseBuilder rb;
        if (isXmlHttpRequest(req) || destURI == null) {
            // Ajax request or no success page: respond with an empty OK.
            // (this reduced traffic a bit as the client probably isn't
            // interested in the redirected page but more in the result
            // of the operation)...
            rb = ok();
        }
        else {
            rb = seeOther(destURI);
        }
        return rb;
    }

    private boolean tokenLifetimeExtended(TokenProperties tokenProps, long maxTokenLifetime) {
        Map<String, String> userIdentity = tokenProps.m_userIdentity;

        String subject = userIdentity.get(SUBJECT);

        RenewalResult result = extendTokenLifetime(userIdentity, maxTokenLifetime);
        if (result == RenewalResult.MAX_LIFETIME_REACHED) {
            // Token is renewed too often...
            log(LOG_INFO, "Not auto-renewing token for %s: maximum token lifetime reached!", subject);

            return false;
        }
        else if (result == RenewalResult.STILL_VALID) {
            // Token is still valid, so nothing needs to be renewed...
            log(LOG_DEBUG, "Not auto-renewing token for %s: token is still valid!", subject);

            return false;
        }

        log(LOG_DEBUG, "Token renewed for %s: token was no longer valid, but could be renewed!", subject);

        // Token was renewed...
        return true;
    }
}
