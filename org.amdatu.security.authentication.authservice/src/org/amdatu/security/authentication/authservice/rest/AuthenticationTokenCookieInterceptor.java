/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest;

import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_SERVER_ERROR;
import static org.amdatu.security.tokenprovider.http.TokenUtil.AMDATU_TOKEN_ATTRIBUTE;
import static org.amdatu.security.tokenprovider.http.TokenUtil.AMDATU_TOKEN_PROPERTIES_ATTRIBUTE;
import static org.amdatu.security.tokenprovider.http.TokenUtil.getTokenFromCookie;
import static org.amdatu.web.util.HttpRequestUtil.isSecureRequest;
import static org.osgi.service.log.LogService.LOG_DEBUG;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.security.authentication.authservice.AuthenticationContext;
import org.amdatu.security.authentication.authservice.AuthenticationException;
import org.amdatu.security.authentication.authservice.AuthenticationInterceptor;
import org.amdatu.security.authentication.authservice.rest.util.TokenUtil;
import org.amdatu.security.authentication.authservice.rest.util.UserAgentUtil;
import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.InvalidTokenException.Reason;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.osgi.service.log.LogService;

/**
 * {@link AuthenticationInterceptor} implementation that sets or removes a token cookie from the servlet response.
 */
public class AuthenticationTokenCookieInterceptor implements AuthenticationInterceptor {
    public static final String ATTR_TOKEN_PROPERTIES = "tokenProperties";
    public static final String ATTR_INVALID_TOKEN_REASON = "invalidTokenReason";

    private final AuthenticationRealmConfig m_config;
    // Injected by Felix DM...
    private volatile TokenProvider m_tokenProvider;
    private volatile LogService m_log;

    /**
     * Creates a new {@link AuthenticationTokenCookieInterceptor} instance.
     */
    public AuthenticationTokenCookieInterceptor(AuthenticationRealmConfig config) {
        m_config = config;
    }

    @Override
    public void onAuthenticationEnd(HttpServletRequest request, HttpServletResponse response) {
        request.removeAttribute(ATTR_TOKEN_PROPERTIES);
        request.removeAttribute(ATTR_INVALID_TOKEN_REASON);
    }

    @Override
    public void onAuthenticationStart(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> properties = null;
        Reason reason = null;

        try {
            String token = getTokenFromCookie(request, m_config.getCookieName());
            properties = m_tokenProvider.verifyToken(token);
        }
        catch (IllegalArgumentException e) {
            // Token wasn't supplied at all...
            reason = Reason.UNKNOWN;
        }
        catch (InvalidTokenException e) {
            // Token has expired or wasn't supplied at all...
            reason = e.getReason();
            properties = e.getAttributes().orElse(null);
        }

        request.setAttribute(ATTR_TOKEN_PROPERTIES, properties);
        request.setAttribute(ATTR_INVALID_TOKEN_REASON, reason);
    }

    @Override
    public void onLogin(AuthenticationContext context, LoginReason reason) {
        m_log.log(LOG_DEBUG, "Setting auth cookie for " + context.getUserIdentity() + " on login. Reason: " + reason);

        Map<String, String> tokenAttrs = context.getUserIdentity();
        Optional<String> token = generateToken(tokenAttrs);

        token
                .map(t -> createAuthCookies(m_config, t, isSecureRequest(context.getServletRequest()), context))
                .orElseGet(() -> createEmptyCookies(m_config, context))
                .forEach(cookie -> context.getServletResponse().addCookie(cookie));

        if (token.isPresent()) {
            context.getServletRequest().setAttribute(AMDATU_TOKEN_ATTRIBUTE, token.get());
            context.getServletRequest().setAttribute(AMDATU_TOKEN_PROPERTIES_ATTRIBUTE, tokenAttrs);
        }
    }

    @Override
    public void onLoginRejected(AuthenticationContext context, RejectReason reason) {
        m_log.log(LOG_DEBUG,
            "Clearing auth cookie for " + context.getUserIdentity() + " on rejected login. Reason: " + reason);

        for (Cookie cookie : createEmptyCookies(m_config, context)) {
            context.getServletResponse().addCookie(cookie);
        }
    }

    @Override
    public void onLogout(AuthenticationContext context, LogoutReason reason) {
        m_log.log(LOG_DEBUG, "Clearing auth cookie for " + context.getUserIdentity() + " on logout. Reason: " + reason);

        for (Cookie cookie : createEmptyCookies(m_config, context)) {
            context.getServletResponse().addCookie(cookie);
        }
    }

    private Optional<String> generateToken(Map<String, String> tokenAttrs) {
        if (tokenAttrs != null && !tokenAttrs.isEmpty()) {
            return Optional.of(m_tokenProvider.generateToken(tokenAttrs));
        }
        return Optional.empty();
    }

    private List<Cookie> createAuthCookies(AuthenticationRealmConfig cfg, String token, boolean secure, AuthenticationContext context) {
        final List<String> chunkedTokens = TokenUtil.splitToken(token);
        if (chunkedTokens.size() > cfg.getMaxCookies()) {
            throw new AuthenticationException(CODE_SERVER_ERROR,
                    "The authentication token couldn't be returned because it exceeded the expected size.");
        }
        return IntStream.range(0, chunkedTokens.size())
                .mapToObj(i -> {
                    Cookie result = new Cookie(cfg.getCookieName() + (i == 0 ? "" : "_" + i), chunkedTokens.get(i));
                    cfg.getCookieDomain()
                            .ifPresent(result::setDomain);
                    result.setMaxAge(cfg.getCookieMaxAge());
                    if (!UserAgentUtil.isSameSiteNoneIncompatible(context.getServletRequest().getHeader("User-Agent"))) {
        				result.setPath(cfg.getCookiePath().concat(" SameSite=none;"));
        			} else {
        				result.setPath(cfg.getCookiePath());
        			}
                    result.setHttpOnly(true); // this is an authentication credential!
                    result.setSecure(secure);
                    return result;
                })
                .collect(Collectors.toList());
    }

    private List<Cookie> createEmptyCookies(AuthenticationRealmConfig cfg, AuthenticationContext context) {
        boolean secure = isSecureRequest(context.getServletRequest());
        int maxAge = 0; // let it be deleted right away!

        return Optional.ofNullable(context.getServletRequest().getCookies())
                .map(Arrays::asList)
                .orElseGet(Collections::emptyList)
                .stream()
                .map(Cookie::getName)
                .filter(cookieName -> cookieName.startsWith(cfg.getCookieName()))
                .map(cookieName -> {
                    Cookie result = new Cookie(cookieName, null);
                    cfg.getCookieDomain()
                            .ifPresent(result::setDomain);
                    if (!UserAgentUtil.isSameSiteNoneIncompatible(context.getServletRequest().getHeader("User-Agent"))) {
        				result.setPath(cfg.getCookiePath().concat(" SameSite=none;"));
        			} else {
        				result.setPath(cfg.getCookiePath());
        			}
                    result.setHttpOnly(true); // this is an authentication credential!
                    result.setSecure(secure);
                    result.setMaxAge(maxAge);
                    return result;
                })
                .collect(Collectors.toList());
    }
}
