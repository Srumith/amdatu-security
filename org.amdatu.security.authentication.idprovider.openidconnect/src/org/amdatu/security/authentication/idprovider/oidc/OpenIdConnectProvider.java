/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc;

import static java.util.stream.Collectors.toMap;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.ACCESS_TOKEN;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_ACCESS_DENIED;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_INVALID_REQUEST_URI;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.ERROR_CODE;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.ID_TOKEN;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROMPT;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_NAME;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_TYPE;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.REFRESH_TOKEN;
import static org.amdatu.security.authentication.idprovider.oidc.OpenIdConnectProviderConfig.TYPE_OPENID_CONNECT;
import static org.amdatu.security.authentication.idprovider.oidc.util.ConfigUtil.getString;
import static org.amdatu.security.authentication.idprovider.oidc.util.ConfigUtil.updateServiceProperties;
import static org.amdatu.security.tokenprovider.TokenConstants.EXPIRATION_TIME;
import static org.osgi.service.log.LogService.LOG_INFO;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.amdatu.security.authentication.idprovider.IdProvider;
import org.amdatu.security.authentication.idprovider.oidc.scribejava.OpenIdConnectService;
import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.web.util.URIBuilder;
import org.apache.felix.dm.Component;
import org.osgi.service.log.LogService;

import com.github.scribejava.core.exceptions.OAuthException;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;

/**
 * Provides a generic OpenID connect-based {@link IdProvider} implementation.
 */
public class OpenIdConnectProvider implements IdProvider {
    private static final String RESPONSE_MODE = "response_mode";
    private static final String NONCE = "nonce";

    // Injected by Felix DM...
    private volatile TokenProvider m_tokenProvider;
    private volatile LogService m_log;
    private volatile Component m_component;
    // Locally managed...
    private volatile OpenIdConnectProviderConfig m_config;

    /**
     * Creates a new {@link OpenIdConnectProvider} instance.
     */
    public OpenIdConnectProvider() {
        // Nop
    }

    @Override
    public URI getAuthenticationURI(URI callbackURI, Map<String, String[]> params) {
        OpenIdConnectProviderConfig cfg = m_config;

        // We'll give each caller its own secret, allowing us to counter XSRF attacks...
        String state = cfg.generateStateCode();

        OAuth20Service oauthSrv = cfg.createOAuthServiceWithState(callbackURI, state);

        Map<String, String> oauthParams = cfg.getAdditionalOAuthParams();
        // If there's a prompt specified in the given parameters, use it as part of the
        // OAuth/OpenID connect authentication URL. It allows us to control the login screen
        // presented to the user when calling the remote OpenID connect provider...
        String[] promptParam = params.getOrDefault(PROMPT, new String[0]);
        if (promptParam.length > 0) {
            oauthParams.put(PROMPT, String.join(" ", promptParam));
        }
        oauthParams.put(RESPONSE_MODE, cfg.getResponseMode());
        oauthParams.put(NONCE, cfg.generateNonce());

        return URI.create(oauthSrv.getAuthorizationUrl(oauthParams));
    }

    @Override
    public Optional<URI> getEndSessionURI(URI callback, Map<String, String> credentials) {
        return getEndSessionURI(callback, credentials, null);
    }

    @Override
    public Optional<URI> getEndSessionURI(URI callback, Map<String, String> credentials, String state) {
        String accessToken = credentials.get(ACCESS_TOKEN);
        if (accessToken == null || "".equals(accessToken)) {
            return Optional.empty();
        }
        String idToken = credentials.get(ID_TOKEN);

        return m_config.getSessionEndURI()
            .map(uriBuilder -> decorateURI(uriBuilder, callback, credentials, state, idToken));
    }

    @Override
    public String getType() {
        return TYPE_OPENID_CONNECT;
    }

    @Override
    public Map<String, String> getUserIdentity(URI callbackURI, Map<String, String[]> params) {
        OpenIdConnectProviderConfig cfg = m_config;

        String providerType = cfg.getName();

        String error = getString(params, "error");
        String state = getString(params, "state");
        String code = getString(params, "code");

        if (error != null) {
            return createMapWithError(error, providerType);
        }

        // Provide an additional check (XSRF)...
        if (state == null || !cfg.verifyStateCode(state)) {
            m_log.log(LOG_WARNING, String.format("Unable to verify callback request for %s from %s: state mismatch! "
                + "Request took either too long or possible replay attack?!", params, providerType));

            return createMapWithError(CODE_INVALID_REQUEST_URI, providerType);
        }

        return getTokenAttributesFromToken(cfg, code, callbackURI, state)
            .orElseGet(() -> createMapWithError(CODE_ACCESS_DENIED, providerType));
    }

    @Override
    public boolean refreshToken(Map<String, String> credentials) throws InvalidTokenException {
        String refreshToken = credentials.get(REFRESH_TOKEN);
        if (refreshToken == null || "".equals(refreshToken.trim())) {
            return false;
        }

        String exp = credentials.get(EXPIRATION_TIME);
        if (exp != null) {
            Instant inst = Instant.ofEpochSecond(Long.valueOf(exp));
            // do not refresh if the token remains valid for at least the configured amount of seconds
            if (Instant.now().plusSeconds(m_config.getRefreshTokenIfExpiresWithinSeconds()).isBefore(inst)) {
                return false;
            }
        }

        OAuth2AccessToken token = null;
        try {
            OAuth20Service oauthSrv = m_config.createOAuthService();
            token = oauthSrv.refreshAccessToken(refreshToken);
        }
        catch (IOException | InterruptedException | ExecutionException | OAuthException e) {
            m_log.log(LOG_WARNING, "Failed to refresh access token!", e);
            return false;
        }

        if (token != null) {
            addTokenAttributes(credentials, token);
            return true;
        }

        return false;
    }

    /**
     * Called by Felix DM.
     */
    protected final void start(Component comp) throws Exception {
        m_log.log(LOG_INFO, "OpenId connect ID provider started for type: " + m_config.getName());
    }

    /**
     * Called by Felix DM.
     */
    protected final void stop(Component comp) throws Exception {
        m_log.log(LOG_INFO, "OpenId connect ID provider stopped for type: " + m_config.getName());
    }

    /**
     * Called by Felix DM.
     */
    protected final void update(Dictionary<String, ?> config) throws Exception {
        OpenIdConnectProviderConfig newConfig = null;
        if (config != null) {
            newConfig = new OpenIdConnectProviderConfig(config);
        }
        m_config = newConfig;

        if (newConfig != null) {
            updateServiceProperties(m_component,
                PROVIDER_TYPE, TYPE_OPENID_CONNECT,
                PROVIDER_NAME, newConfig.getName(),
                "issuer", newConfig.getIdTokenIssuer(),
                "url", newConfig.getIdTokenJwksUri());
        }
    }

    private URI decorateURI(URIBuilder builder, URI callback, Map<String, String> credentials, String state, String idTokenHint) {
        if (callback != null) {
            builder.appendToQuery("post_logout_redirect_uri", callback.toASCIIString());
        }
        if (state != null) {
            builder.appendToQuery("state", state);
        }
        if (idTokenHint != null) {
            builder.appendToQuery("id_token_hint", idTokenHint);
        }
        return builder.build();
    }

    private Optional<Map<String, String>> getTokenAttributesFromToken(OpenIdConnectProviderConfig cfg, String code,
        URI callbackURI, String state) {
        OpenIdConnectService oidcService = cfg.createOAuthServiceWithState(callbackURI, state);

        OAuth2AccessToken token;
        try {
            token = oidcService.getAccessToken(code);
        }
        catch (IOException | InterruptedException | ExecutionException | OAuthException e) {
            m_log.log(LOG_WARNING, "Failed to obtain access token! Denying access!", e);

            return Optional.empty();
        }
       
        String idToken = token.getParameter(ID_TOKEN);
        if (idToken == null) {
            m_log.log(LOG_WARNING, "Unable to retrieve id-token from access token! Denying access!");

            return Optional.empty();
        }

        // shouldn't we try catch here and return access denied if it fails?
        Map<String, String> attrs = m_tokenProvider.verifyToken(idToken);

        String givenNonce = attrs.get(NONCE);
        if (givenNonce == null || !cfg.verifyNonce(givenNonce)) {
            m_log.log(LOG_WARNING, "Unable to verify nonce! Denying access!");

            return Optional.empty();
        }

        if (cfg.isCallUserInfoEndpoint()) {
            try {
                attrs.putAll(oidcService.getUserInfo(token.getAccessToken()));
            }
            catch (IOException | InterruptedException | ExecutionException | OAuthException e) {
                m_log.log(LOG_WARNING, "Failed to obtain user information! Denying access!", e);

                return Optional.empty();
            }
        }

        Map<String, String> tokenAttrs = extractWantedClaims(cfg.getClaims(), attrs);
        // Add/override the properties we want to include as well...
        tokenAttrs.put(PROVIDER_TYPE, TYPE_OPENID_CONNECT);
        tokenAttrs.put(PROVIDER_NAME, cfg.getName());
        tokenAttrs.put(ID_TOKEN, idToken);
        
        addTokenAttributes(tokenAttrs, token);

        return Optional.of(tokenAttrs);
    }

    private void addTokenAttributes(Map<String, String> attributes, OAuth2AccessToken token) {
        attributes.put(ACCESS_TOKEN, token.getAccessToken());
        if (token.getExpiresIn() != null) {
            attributes.put(EXPIRATION_TIME,
                Long.toString(Instant.now().plusSeconds(token.getExpiresIn()).getEpochSecond()));
        }
        String refreshToken = token.getRefreshToken();
        if (refreshToken != null && !"".equals(refreshToken)) {
            attributes.put(REFRESH_TOKEN, refreshToken);
        }
    }

    private Map<String, String> extractWantedClaims(List<String> wantedClaims, Map<String, String> attrs) {
        return attrs.entrySet().stream()
            .filter(e -> wantedClaims.contains(e.getKey()))
            .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private static Map<String, String> createMapWithError(String error, String providerName) {
        Map<String, String> tokenAttrs = new HashMap<>();
        tokenAttrs.put(PROVIDER_TYPE, TYPE_OPENID_CONNECT);
        tokenAttrs.put(PROVIDER_NAME, providerName);
        tokenAttrs.put(ERROR_CODE, error);
        return tokenAttrs;
    }
}
