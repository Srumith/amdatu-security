/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc;

import static com.github.scribejava.httpclient.ning.NingHttpClientConfig.defaultConfig;
import static java.util.Collections.list;
import static org.amdatu.security.authentication.idprovider.oidc.scribejava.OpenIdConnectConfig.parseOICConfig;
import static org.amdatu.security.authentication.idprovider.oidc.util.ConfigUtil.getBoolean;
import static org.amdatu.security.authentication.idprovider.oidc.util.ConfigUtil.getInteger;
import static org.amdatu.security.authentication.idprovider.oidc.util.ConfigUtil.getMandatoryString;
import static org.amdatu.security.authentication.idprovider.oidc.util.ConfigUtil.getString;
import static org.amdatu.security.authentication.idprovider.oidc.util.ConfigUtil.getStringArray;
import static org.amdatu.security.authentication.idprovider.oidc.util.ConfigUtil.getURI;
import static org.amdatu.security.util.crypto.CryptoUtils.createSecureRandom;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.amdatu.security.authentication.idprovider.oidc.scribejava.OpenIdConnectApi;
import org.amdatu.security.authentication.idprovider.oidc.scribejava.OpenIdConnectConfig;
import org.amdatu.security.authentication.idprovider.oidc.scribejava.OpenIdConnectService;
import org.amdatu.security.password.util.Totp;
import org.amdatu.web.util.URIBuilder;
import org.osgi.service.cm.ConfigurationException;

import com.github.scribejava.core.httpclient.HttpClient;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.httpclient.ning.NingProvider;

/**
 * Represents the {@link OpenIdConnectProvider} configuration.
 */
public class OpenIdConnectProviderConfig {
    /**
     * REQUIRED. The remote ID provider name, like "google", used to distinguish between multiple OpenID Connect providers.
     */
    public static final String KEY_NAME = "name";
    /**
     * REQUIRED. The client or API identifier for the remote ID provider.
     */
    public static final String KEY_CLIENT_ID = "clientId";
    /**
     * REQUIRED. The client or API secret for the remote ID provider.
     */
    public static final String KEY_CLIENT_SECRET = "clientSecret";
    /**
     * OPTIONAL. The claims to copy from the ID token, if this one is returned.
     */
    public static final String KEY_CLAIMS = "claims";
    /**
     * OPTIONAL. The JSON-string describing the OpenID connect discovery information. If omitted uses either the URL provided by {@link #KEY_OPEN_ID_CONFIG_URL} or determines it based on the given {@link #KEY_NAME}.
     */
    public static final String KEY_OPEN_ID_CONFIG = "openIdConfig";
    /**
     * OPTIONAL. The URL to the OpenID connect discovery endpoint. If omitted uses either the {@link #KEY_OPEN_ID_CONFIG} or determines the endpoint based on the {@link #KEY_NAME}.
     */
    public static final String KEY_OPEN_ID_CONFIG_URL = "openIdConfigURL";
    /**
     * OPTIONAL. The type of response to request from the remote ID provider. Defaults to <tt>code</tt>.
     */
    public static final String KEY_RESPONSE_TYPE = "responseType";
    /**
     * OPTIONAL. The scopes to request access to. Multiple scopes should be separated by single spaces. Defaults to <tt>openid email profile</tt>.
     */
    public static final String KEY_SCOPES = "scopes";
    /**
     * OPTIONAL. The algorithm used for the {@link SecureRandom} instance. Defaults to using the default algorithm selected by {@code new SecureRandom()}.
     */
    public static final String KEY_PRNG_ALGORITHM = "secureRandomAlgorithm";
    /**
     * OPTIONAL. The response mode in which the authorization result response is returned. Only <tt>query</tt> and <tt>form_post</tt>
     * are supported. Defaults to <tt>query</tt>.
     */
    public static final String KEY_RESPONSE_MODE = "responseMode";
    /**
     * OPTIONAL. If set to <tt>true</tt>, the UserInfo endpoint of the OpenID connect provider is called to augment the attributes of the
     * id-token with extra user information. Defaults to <tt>true</tt>.
     */
    public static final String KEY_CALL_USER_INFO_ENDPOINT = "callUserInfoEndpoint";
    /**
     * OPTIONAL. The amount of time (in seconds) a state code is valid. Defaults to 120 seconds.
     */
    public static final String KEY_STATE_TIMEOUT = "stateTimeout";
    /**
     * OPTIONAL. The minimum time that an access token should be valid, before refreshing it. Defaults to 60 seconds.
     */
    public static final String REFRESH_TOKEN_IF_EXPIRES_WITHIN_SECONDS = "refreshTokenIfExpiresWithinSeconds";

    public static final String TYPE_OPENID_CONNECT = "oidc";

    /**
     * Prefix used to pass additional parameters to the OAuth authorization URL.
     */
    private static final String KEY_EXTRA_PARAM_PREFIX = "param.";

    public static final String TYPE_GOOGLE = "google";
    public static final String TYPE_OFFICE = "office";

    private static final String DEFAULT_SCOPES = "openid email profile";
    private static final String DEFAULT_RESPONSE_TYPE = "code";
    private static final String[] DEFAULT_STD_CLAIMS = {
        "sub", "name", "given_name", "family_name", "middle_name", "nickname", "preferred_username", "profile",
        "picture", "website", "email", "email_verified", "gender", "birthdate", "zoneinfo", "locale",
        "phone_number", "phone_number_verified", "address", "updated_at",
        // default claims used by Microsoft OAuth endpoints
        "upn", "unique_name",
    };
    private static final String DEFAULT_RESPONSE_MODE = "query";
    private static final int DEFAULT_STATE_TIMEOUT = 120; // seconds
    private static final boolean DEFAULT_CALL_USER_INFO_ENDPOINT = false;
    private static final int DEFAULT_REFRESH_TOKEN_IF_EXPIRES_WITHIN_SECONDS = 60; // seconds

    private static final int TOTP_SECRET_LENGTH = 24;
    private static final int STATE_SALT_LENGTH = 8;
    private static final int NONCE_SALT_LENGTH = 16;

    /** OpenID connect discovery URL for Google. */
    public static final String GOOGLE_OPENID_CONF =
        "https://accounts.google.com/.well-known/openid-configuration";
    /** OpenID connect discovery URL for Office365. */
    public static final String OFFICE_OPENID_CONF =
        "https://login.microsoftonline.com/common/.well-known/openid-configuration";

    private final String m_name;
    private final String m_apiKey;
    private final String m_apiSecret;
    private final String m_responseType;
    private final String m_scopes;
    private final Map<String, String> m_extraAuthParams;
    private final List<String> m_claims;
    private final boolean m_callUserInfoEndpoint;
    private final String m_responseMode;
    private final OpenIdConnectConfig m_apiConfig;
    private final SecureRandom m_prng;
    private final Totp m_totp;
    private final int m_refreshTokenIfExpiresWithinSeconds;

    /**
     * Creates a new {@link OpenIdConnectProviderConfig} instance.
     */
    public OpenIdConnectProviderConfig(Dictionary<String, ?> config) throws ConfigurationException, IOException {
        m_name = getMandatoryString(config, KEY_NAME);

        try {
            m_prng = createSecureRandom(getString(config, KEY_PRNG_ALGORITHM, null));
        }
        catch (NoSuchAlgorithmException e) {
            throw new ConfigurationException(KEY_PRNG_ALGORITHM, "unsupported algorithm!", e);
        }

        m_apiKey = getMandatoryString(config, KEY_CLIENT_ID);
        m_apiSecret = getMandatoryString(config, KEY_CLIENT_SECRET);
        m_responseType = getString(config, KEY_RESPONSE_TYPE, DEFAULT_RESPONSE_TYPE);
        m_scopes = getString(config, KEY_SCOPES, DEFAULT_SCOPES);

        m_claims = Arrays.asList(getStringArray(config, KEY_CLAIMS, DEFAULT_STD_CLAIMS));
        m_callUserInfoEndpoint = getBoolean(config, KEY_CALL_USER_INFO_ENDPOINT, DEFAULT_CALL_USER_INFO_ENDPOINT);
        m_responseMode = getString(config, KEY_RESPONSE_MODE, DEFAULT_RESPONSE_MODE);

        int stateTimeout = getInteger(config, KEY_STATE_TIMEOUT, DEFAULT_STATE_TIMEOUT);
        m_refreshTokenIfExpiresWithinSeconds =
            getInteger(config, REFRESH_TOKEN_IF_EXPIRES_WITHIN_SECONDS, DEFAULT_REFRESH_TOKEN_IF_EXPIRES_WITHIN_SECONDS);

        m_extraAuthParams = new HashMap<>();
        for (String key : list(config.keys())) {
            if (!key.startsWith(KEY_EXTRA_PARAM_PREFIX)) {
                continue;
            }
            String val = getString(config, key, null);
            if (val != null) {
                m_extraAuthParams.put(key.substring(KEY_EXTRA_PARAM_PREFIX.length()), val);
            }
        }

        m_apiConfig = getOpenIdConnectConfig(config);

        m_totp = new Totp.Builder()
            .deriveSecret(m_apiSecret, m_apiKey, TOTP_SECRET_LENGTH)
            .setTimeStep(stateTimeout)
            .build();
    }

    static OpenIdConnectConfig getOpenIdConnectConfig(Dictionary<String, ?> dict)
        throws ConfigurationException, IOException {
        String config = getString(dict, KEY_OPEN_ID_CONFIG, null);
        if (config != null) {
            return parseOICConfig(new ByteArrayInputStream(config.getBytes()));
        }
        URI configURI = getURI(dict, KEY_OPEN_ID_CONFIG_URL, null);
        if (configURI == null) {
            String type = getMandatoryString(dict, KEY_NAME).toLowerCase();
            if (TYPE_GOOGLE.equals(type)) {
                configURI = URI.create(GOOGLE_OPENID_CONF);
            }
            else if (TYPE_OFFICE.equals(type)) {
                configURI = URI.create(OFFICE_OPENID_CONF);
            }
        }
        if (configURI == null) {
            throw new ConfigurationException(null,
                "neither '" + KEY_OPEN_ID_CONFIG + "' nor '" + KEY_OPEN_ID_CONFIG_URL + "' is supplied!");
        }

        return parseOICConfig(configURI);
    }

    public OpenIdConnectService createOAuthService() {
        return createOAuthServiceWithState(null, null);
    }

    public OpenIdConnectService createOAuthServiceWithState(URI callbackURI, String state) {
        HttpClient httpClient = new NingProvider().createClient(defaultConfig());

        String callback = null;
        if (callbackURI != null) {
            callback = callbackURI.toASCIIString();
        }

        OAuthConfig oauthConfig = new OAuthConfig(m_apiKey, m_apiSecret, callback, m_scopes, null /* debugStream */,
            state, m_responseType, null /* userAgent */, null /* httpClientConfig */, httpClient);

        return new OpenIdConnectApi(m_apiConfig).createService(oauthConfig);
    }

    public String generateNonce() {
        return generateTotpCodeWithRandomSalt(NONCE_SALT_LENGTH);
    }

    public String generateStateCode() {
        return generateTotpCodeWithRandomSalt(STATE_SALT_LENGTH);
    }

    public Map<String, String> getAdditionalOAuthParams() {
        return new HashMap<>(m_extraAuthParams);
    }

    /**
     * @return the claims to extract from the ID token, never <code>null</code>.
     */
    public List<String> getClaims() {
        return m_claims;
    }

    /**
     * @return the issuer of the ID tokens returned by the OpenID connect provider.
     */
    public String getIdTokenIssuer() {
        String issuer = m_apiConfig.getIssuer();
        Pattern p = Pattern.compile("\\{[^}]+\\}");
        Matcher m = p.matcher(issuer);
        return m.replaceAll(".*?");
    }

    /**
     * @return the URI to the JSON web keys that can be used to validate the ID tokens, never <code>null</code>.
     */
    public String getIdTokenJwksUri() {
        return m_apiConfig.getJwksUri();
    }

    /**
     * @return the response mode, cannot be <code>null</code>.
     */
    public String getResponseMode() {
        return m_responseMode;
    }

    /**
     * @return an optional URL to use for logging out the user.
     */
    public Optional<URIBuilder> getSessionEndURI() {
        String url = m_apiConfig.getEndSessionEndpoint();
        if (url == null || "".equals(url)) {
            return Optional.empty();
        }
        return Optional.of(new URIBuilder(url));
    }

    /**
     * @return the name of OpenID connect provider, used to find back a particular provider.
     */
    public String getName() {
        return m_name;
    }

    /**
     * @return <code>true</code> if the UserInfo endpoint should be called to extra user information, <code>false</code> if this call should be omitted.
     */
    public boolean isCallUserInfoEndpoint() {
        return m_callUserInfoEndpoint;
    }

    public boolean verifyNonce(String code) {
        return verifyTotpCode(code, NONCE_SALT_LENGTH);
    }

    public boolean verifyStateCode(String code) {
        return verifyTotpCode(code, STATE_SALT_LENGTH);
    }

    public int getRefreshTokenIfExpiresWithinSeconds() {
        return m_refreshTokenIfExpiresWithinSeconds;
    }

    private String generateTotpCodeWithRandomSalt(int saltLength) {
        byte[] salt = new byte[saltLength];
        m_prng.nextBytes(salt);

        byte[] dest = new byte[m_totp.getMacLength() + salt.length];

        m_totp.generateRawCode(salt, dest);
        // append the salt for our own use later on...
        System.arraycopy(salt, 0, dest, dest.length - salt.length, salt.length);

        return Base64.getUrlEncoder().withoutPadding().encodeToString(dest);
    }

    private boolean verifyTotpCode(String code, int saltLength) {
        byte[] data = Base64.getUrlDecoder().decode(code);
        if (data.length < saltLength) {
            return false;
        }

        byte[] salt = Arrays.copyOfRange(data, data.length - saltLength, data.length);
        byte[] rawCode = Arrays.copyOf(data, data.length - saltLength);

        return m_totp.validateRawCode(rawCode, salt);
    }
}
