/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.util.crypto;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.spec.SecretKeySpec;

/**
 * Provides various crypto-related utility methods.
 */
public final class CryptoUtils {
    private static final int DEFAULT_SECRET_LENGTH = 64; // bytes

    /**
     * @param algName the name of the secure random algorithm, can be <code>null</code> to use the platform default.
     * @return a secure random instance, never <code>null</code>.
     * @see {@link https://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#SecureRandom}
     */
    public static SecureRandom createSecureRandom(String algName) throws NoSuchAlgorithmException {
        if (algName == null) {
            return new SecureRandom();
        }
        return SecureRandom.getInstance(algName);
    }

    /**
     * @param prng the secure number generator that is to be used for generating random bytes;
     * @param len the amount of random bytes to return;
     * @return the requested number of random bytes, as array, never <code>null</code>.
     */
    public static byte[] generateSecret(SecureRandom prng, int len) {
        byte[] buf = new byte[len];
        prng.nextBytes(buf);
        return buf;
    }

    /**
     * Returns key material for a secret key from a given URI.
     * <p>
     * This method supports the following URI schemes:
     * <ul>
     * <li><tt>random:&lt;key size&gt;</tt> for a randomly generated key of a given size;
     * <li><tt>data:&lt;base64 encoded data&gt;</tt> for a predefined static key;
     * <li><tt>file://path/to/key.file</tt> for a key stored in an external file.
     * </ul>
     *
     * @param prng the secure number generator that is to be used for generating random key material;
     * @param keyUri the URI representing the key data, or its location, cannot be <code>null</code>;
     * @return a secret key for HMAC signing purposes, never <code>null</code>.
     * @throws IOException in case of I/O problems reading the key material from disk.
     */
    public static byte[] getSecretKey(SecureRandom prng, URI keyUri) throws IOException {
        byte[] key;
        switch (keyUri.getScheme()) {
            case "random":
                int length = parseInteger(keyUri.getSchemeSpecificPart(), DEFAULT_SECRET_LENGTH);
                key = generateSecret(prng, length);
                break;
            case "data":
                key = Base64.getDecoder().decode(keyUri.getSchemeSpecificPart());
                break;
            case "file":
                key = Files.readAllBytes(new File(keyUri).toPath());
                break;
            default:
                throw new IOException("Unsupported key URI: " + keyUri);
        }
        return key;
    }

    /**
     * @param prng the secure number generator that is to be used for generating random key material;
     * @param keyUri the URI representing the key data, or its location, cannot be <code>null</code>;
     * @param keySize the key size, in <b>bits</b> of the HMAC key to return.
     * @return a new HMAC key instance, never <code>null</code>.
     * @throws IOException in case of I/O problems reading the key material from disk.
     * @see #getSecretKey(SecureRandom, URI)
     */
    public static Key getHmacKey(SecureRandom prng, URI keyUri, int keySize) throws IOException {
        int byteCount = keySize / 8;
        byte[] key = getSecretKey(prng, keyUri);
        return new SecretKeySpec(truncate(key, byteCount), "HMAC");
    }

    private static byte[] truncate(byte[] input, int maxLen) {
        return Arrays.copyOf(input, Math.min(maxLen, input.length));
    }

    private static int parseInteger(String val, int dflt) {
        if (val == null || "".equals(val)) {
            return dflt;
        }
        try {
            return Integer.parseInt(val);
        }
        catch (NumberFormatException e) {
            return dflt;
        }
    }
}
