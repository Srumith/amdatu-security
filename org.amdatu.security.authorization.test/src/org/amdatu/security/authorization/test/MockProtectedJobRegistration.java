/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

public final class MockProtectedJobRegistration {
    private final String m_identifier;

    public MockProtectedJobRegistration(String identifier) {
        m_identifier = identifier;
    }

    public String getIdentifier() {
        return m_identifier;
    }

    public String getCompanyIdentifier() {
        return "9876";
    }

    public String getPassportId() {
        return "ep123456782";
    }
}
