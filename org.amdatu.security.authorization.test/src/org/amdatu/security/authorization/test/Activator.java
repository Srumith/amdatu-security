/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.security.authorization.EntityProvider;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Component mockPassportService = createComponent()
            .setInterface(new String[] { MockProtectedPassportService.class.getName(), AuthorizationPolicyProvider.class.getName(), EntityProvider.class.getName() }, null)
            .setImplementation(new MockProtectedPassportService())
            .add(createServiceDependency().setService(AuthorizationService.class).setRequired(true));
        manager.add(mockPassportService);

        Component mockPersonService = createComponent()
            .setInterface(new String[] { MockProtectedPersonService.class.getName(), AuthorizationPolicyProvider.class.getName(), EntityProvider.class.getName() }, null)
            .setImplementation(new MockProtectedPersonService())
            .add(createServiceDependency().setService(AuthorizationService.class).setRequired(true));
        manager.add(mockPersonService);

        Component mockContextValueProvider = createComponent()
            .setInterface(EntityProvider.class.getName(), null)
            .setImplementation(new MockContextProvider());
        manager.add(mockContextValueProvider);
    }
}
