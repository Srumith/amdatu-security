/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.test.idprovider;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.amdatu.security.authentication.authservice.PrincipalLookupService;
import org.amdatu.security.authentication.idprovider.local.LocalIdCredentialsProvider;
import org.amdatu.security.authentication.test.app.MyAuthService;
import org.amdatu.security.authentication.test.app.MyLocalApp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.web.testing.http.HttpTestConfigurator.createWaitForHttpEndpoints;
import static org.amdatu.web.testing.http.HttpTestConfigurator.useLocalCookieJar;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

/**
 * Test cases for LocalIdProvider.
 */
public class LocalIdProviderTest {
    public static final String[] KNOWN_USERS = { "known_user@localhost" };

    private final MyAuthService m_authService = new MyAuthService(KNOWN_USERS);
    private final MyLocalApp m_app = new MyLocalApp();

    private String m_baseURI;

    @Before
    public void setUp() throws InterruptedException {
        Properties appProps = new Properties();
        appProps.put(JAX_RS_RESOURCE, "true");

        int port = Integer.getInteger("org.osgi.service.http.port", 8080);
        m_baseURI = String.format("http://localhost:%d/", port);

        configure(this)
            .add(createConfiguration("org.amdatu.security.account.admin")
                .set("keyAccountId", "email"))
            .add(createFactoryConfiguration("org.amdatu.security.authentication.idprovider.local")
                .set("name", "local",
                    "credentialKeys", "email, password",
                    "signingKeyUri", "random:32",
                    "timestepWindow", "2"))
            .add(createFactoryConfiguration("org.amdatu.security.authentication")
                .set("realm", "integration.test",
                    "osgi.http.whiteboard.context.name", "auth.test",
                    "osgi.http.whiteboard.context.path", "/authtest",
                    "idProviderFilter", "(providerType=local)",
                    "preLoginURL", m_baseURI + "authtest/index.html",
                    "landingPageURL", m_baseURI + "rest/my/app",
                    "failureURL", m_baseURI + "rest/my/failure"))
            .add(createComponent()
                .setInterface(Object.class.getName(), appProps)
                .setImplementation(m_app))
            .add(createComponent()
                .setInterface(LocalIdCredentialsProvider.class.getName(), null)
                .setImplementation(m_authService))
            .add(createComponent()
                .setInterface(PrincipalLookupService.class.getName(), null)
                .setImplementation(m_authService))
            .add(createWaitForHttpEndpoints(m_baseURI + "authtest/rest/login/providers"))
            .add(useLocalCookieJar())
            .apply();
    }

    @After
    public void tearDown() {
        cleanUp(this);
    }

    @Test
    public void testUseLocalIdProviderOk() throws Exception {
        FormBody formBody = new FormBody.Builder()
                .add("email", KNOWN_USERS[0])
                .add("password", "secret")
                .add("providerName", "local")
                .build();



        Request request = new Request.Builder()
                .url(m_baseURI + "authtest/rest/login")
                .post(formBody)
                .build();

        OkHttpClient client = new OkHttpClient();
        Response response = client.newCall(request).execute();
        assertEquals(200, response.code());
        assertEquals("secure resource", response.body().string());

        assertTrue(m_app.m_appCalled.await(5, TimeUnit.SECONDS));
        assertFalse(m_app.m_failureCalled.await(1, TimeUnit.MILLISECONDS));
    }

    @Test
    public void testUseLocalIdProvider_UnknownUser() throws Exception {
        FormBody formBody = new FormBody.Builder()
                .add("email", "unknown@localhost")
                .add("password", "does_not_matter")
                .add("providerName", "local")
                .build();

        Request request = new Request.Builder()
                .url(m_baseURI + "authtest/rest/login")
                .post(formBody)
                .build();

        OkHttpClient client = new OkHttpClient();
        Response response = client.newCall(request).execute();
        assertEquals(200, response.code());

        assertTrue(m_app.m_failureCalled.await(5, TimeUnit.SECONDS));
        assertFalse(m_app.m_appCalled.await(1, TimeUnit.MILLISECONDS));
    }

    @Test
    public void testUseLocalIdProviderWithExpiredAuthURL() throws Exception {
        FormBody formBody = new FormBody.Builder()
                .add("email", KNOWN_USERS[0])
                .add("password", "secret")
                .add("providerName", "local")
                .build();

        Request request = new Request.Builder()
                .url(m_baseURI + "authtest/rest/login")
                .post(formBody)
                .build();

        OkHttpClient client = new OkHttpClient.Builder().followRedirects(false).build();

        Response response = client.newCall(request).execute();
        assertEquals(303, response.code());

        String location = response.header("Location");
        assertNotNull(location);

        TimeUnit.SECONDS.sleep(2L); // should invalidate our code...

        Response getResponse = new OkHttpClient().newCall(new Request.Builder().url(location).get().build()).execute();
        assertEquals(200, getResponse.code());

        assertTrue(m_app.m_failureCalled.await(5, TimeUnit.SECONDS));
        assertFalse(m_app.m_appCalled.await(1, TimeUnit.MILLISECONDS));
    }
}
