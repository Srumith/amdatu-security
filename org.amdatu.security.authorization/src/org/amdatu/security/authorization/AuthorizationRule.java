/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Represents an authorization rule in an {@link AuthorizationPolicy}.
 */
@ConsumerType
public interface AuthorizationRule {
    /**
     * @return the entities related to the subject that this rule needs
     */
    public List<EntitySelector> getSubjectEntitySelectors();

    /**
     * @return the entities related to the resource that this rule needs
     */
    public List<EntitySelector> getResourceEntitySelectors();

    /**
     * Evaluate this rule for the specified subject and resource entities.
     *
     * @param subjectEntities
     *            the subject entities, selected based on {@link #getSubjectEntitySelectors()}
     * @param resourceEntities
     *            the resource entities, selected based on {@link #getResourceEntitySelectors()}
     * @return an Optional boolean that is either true, false or empty, depending on whether this rule decides that
     *         access should be granted, denied or could not be determined, respectively.
     */
    public Optional<Boolean> evaluate(Map<String, Object> subjectEntities, Map<String, Object> resourceEntities);
}
