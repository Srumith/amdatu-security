/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Represents a generic way to get entities based on type and properties.
 *
 * @param <T>
 *            the type of entity provided by this EntityProvider
 */
@ConsumerType
public interface EntityProvider<T> {

	/**
	 * Returns the type of entity this entity provider is capable of resolving.
	 * @return
	 */
	public String getEntityType();

	/**
	 * Dictionary like approach exposing the properties the provider needs.
	 * @return
	 */
	public Collection<String> getRequiredPropertyKeys();

    /**
     * Get an entity for the specified selector. If this provider is not able to provide an entity, return an empty
     * Optional.
     * <p>
     * Implementation note: as this method is likely to be called often, it is recommended to implement a short-cut that
     * returns an empty set if this provider does not provide policies for the specified parameters.
     *
     * @param properties offering the properties required by this entity-resolver to resolve the entity.
     * @return an Optional entity for the specified selector
     */
    public Optional<T> getEntity(Map<String, Object> properties);
}
