/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.impl;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.amdatu.security.authorization.Action;
import org.amdatu.security.authorization.AttributeQualifier;
import org.amdatu.security.authorization.AuthorizationException;
import org.amdatu.security.authorization.AuthorizationPolicy;
import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.AuthorizationRule;
import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.security.authorization.EntityProvider;
import org.amdatu.security.authorization.EntitySelector;
import org.amdatu.security.authorization.UnauthorizedException;
import org.osgi.framework.ServiceReference;

/**
 * AuthorizationService implementation based on policy providers and entity providers.
 *
 * <p>
 * The basic premise of this implementation is:
 * <ul>
 * <li>if any rule from any applicable policy returns false, authorization is denied
 * <li>or else if at least one rule from any applicable policy returns true, authorization is granted
 * <li>or else if there are no applicable policies or if there is no rule that returns a definitive answer,
 * authorization is denied
 * </ul>
 */
public class AuthorizationServiceImpl implements AuthorizationService {

    private final Map<ServiceReference<AuthorizationPolicyProvider>, AuthorizationPolicyProvider> policyProviders = new ConcurrentHashMap<>();

    public void addPolicyProvider(ServiceReference<AuthorizationPolicyProvider> ref, AuthorizationPolicyProvider provider) {
        policyProviders.put(ref, provider);
    }

    public void removePolicyProvider(ServiceReference<AuthorizationPolicyProvider> ref, AuthorizationPolicyProvider provider) {
        policyProviders.remove(ref, provider);
    }

    private final SortedMap<ServiceReference<EntityProvider<?>>, EntityProvider<?>> entityProviders = new ConcurrentSkipListMap<>();

    public void addEntityProvider(ServiceReference<EntityProvider<?>> ref, EntityProvider<?> provider) {
        entityProviders.put(ref, provider);
    }

    public void removeEntityProvider(ServiceReference<EntityProvider<?>> ref, EntityProvider<?> provider) {
        entityProviders.remove(ref, provider);
    }

    @Override
    public boolean isAuthorized(String subjectType, Map<String, Object> subjectAttributes, Action action,
        String resourceType, Optional<Map<String, Object>> resourceAttributes) {
        return new Authorizer(subjectType, subjectAttributes, Optional.of(action), resourceType, resourceAttributes).isAuthorized();
    }

    @Override
    public boolean isAuthorized(String subjectType, String subjectIdentifier, Action action, String resourceType, Optional<String> resourceIdentifier) {
        Map<String, Object> subjectAttributes = Collections.singletonMap(AuthorizationService.SUBJECT_IDENTIFIER, subjectIdentifier);
        Optional<Map<String, Object>> resourceAttributes = resourceIdentifier.map(id -> Collections.singletonMap(AuthorizationService.RESOURCE_IDENTIFIER, id));
        return new Authorizer(subjectType, subjectAttributes, Optional.of(action), resourceType, resourceAttributes).isAuthorized();
    }

    @Override
    public void checkAuthorization(String subjectType, Map<String, Object> subjectAttributes, Action action,
        String resourceType, Optional<Map<String, Object>> resourceAttributes) {

        if (!isAuthorized(subjectType, subjectAttributes, action, resourceType, resourceAttributes)) {
            throw new UnauthorizedException();
        }
    }

    @Override
    public void checkAuthorization(String subjectType, String subjectIdentifier, Action action, String resourceType,
        Optional<String> resourceIdentifier) {

        if (!isAuthorized(subjectType, subjectIdentifier, action, resourceType, resourceIdentifier)) {
            throw new UnauthorizedException();
        }
    }

    @Override
    public Set<Action> getAuthorizedActions(String subjectType, Map<String, Object> subjectAttributes, String resourceType, Optional<Map<String, Object>> resourceAttributes) {
        return new Authorizer(subjectType, subjectAttributes, Optional.empty(), resourceType, resourceAttributes).getAuthorizedActions();
    }

    @Override
    public Set<Action> getAuthorizedActions(String subjectType, String subjectIdentifier, String resourceType, Optional<String> resourceIdentifier) {
        Map<String, Object> subjectAttributes = Collections.singletonMap(AuthorizationService.SUBJECT_IDENTIFIER, subjectIdentifier);
        Optional<Map<String, Object>> resourceAttributes = resourceIdentifier.map(id -> Collections.singletonMap(AuthorizationService.RESOURCE_IDENTIFIER, id));
        return new Authorizer(subjectType, subjectAttributes, Optional.empty(), resourceType, resourceAttributes).getAuthorizedActions();
    }

    /**
     * Used to maintain state/cache selected entities for one request.
     */
    private class Authorizer {
        private final String subjectType;
        private final Map<String, Object> subjectAttributes;
        private final Optional<Action> action;
        private final String resourceType;
        private final Map<String, Object> resourceAttributes;

        private final ConcurrentMap<EntitySelector, Object> selectedEntities = new ConcurrentHashMap<>();

        private Authorizer(String subjecType, Map<String, Object> subjectAttributes, Optional<Action> action, String resourceType, Optional<Map<String, Object>> resourceAttributes) {
            this.subjectType = subjecType;
            this.action = action;
            this.resourceType = resourceType;
            this.subjectAttributes = subjectAttributes;
            // create from optional
            this.resourceAttributes = new HashMap<>();
            resourceAttributes.ifPresent(this.resourceAttributes::putAll);
        }

        public boolean isAuthorized() {
            Stream<AuthorizationPolicy> policies = getApplicablePoliciesForSubjectResourceAndAction();
            Stream<AuthorizationRule> rules = policies.flatMap(policy -> policy.getRules().stream());
            Optional<Boolean> result = evaluateRules(rules);
            return result.orElse(false);
        }

        public Set<Action> getAuthorizedActions() {
            ConcurrentMap<Optional<Boolean>, List<AuthorizationPolicy>> policiesByAuthorizationResult =
                getApplicablePoliciesForSubjectAndResource().collect(Collectors.groupingByConcurrent(this::evaluatePolicy));

            Set<Action> authorizedActions = policiesByAuthorizationResult.getOrDefault(Optional.of(true), Collections.emptyList()).stream().flatMap(policy -> policy.getActions().stream())
                .collect(Collectors.toCollection(HashSet::new));
            Set<Action> unauthorizedActions =
                policiesByAuthorizationResult.getOrDefault(Optional.of(false), Collections.emptyList()).stream().flatMap(policy -> policy.getActions().stream()).collect(Collectors.toSet());
            authorizedActions.removeAll(unauthorizedActions);

            return authorizedActions;
        }

        private Stream<AuthorizationPolicy> getApplicablePoliciesForSubjectAndResource() {
            return policyProviders.values().stream().flatMap(provider -> provider.getPolicies().stream()).filter(this::isApplicableSubjectResource);
        }

        private Stream<AuthorizationPolicy> getApplicablePoliciesForSubjectResourceAndAction() {
            return policyProviders.values().stream().flatMap(provider -> provider.getPolicies().stream()).filter(this::isApplicableSubjectResourceAction);
        }

        private boolean isApplicableSubjectResource(AuthorizationPolicy policy) {
            if (!(policy.getSubjectDescriptor().getEntityTypes().isEmpty() || policy.getSubjectDescriptor().getEntityTypes().contains(subjectType))) {
                return false;
            }

            if (!(policy.getResourceDescriptor().getEntityTypes().isEmpty() || policy.getResourceDescriptor().getEntityTypes().contains(resourceType))) {
                return false;
            }

            if (!policy.getSubjectDescriptor().getAttributeQualifiers().stream().allMatch(qualifier -> this.qualifies(qualifier, subjectAttributes))) {
                return false;
            }

            if (!policy.getResourceDescriptor().getAttributeQualifiers().stream().allMatch(qualifier -> this.qualifies(qualifier, resourceAttributes))) {
                return false;
            }

            return true;
        }

        private boolean isApplicableSubjectResourceAction(AuthorizationPolicy policy) {
            boolean actionApplies = policy.getActions().contains(action.get()); // Optional can not be null here since this method is always called though getAuthorized.
            if (actionApplies) {
                return isApplicableSubjectResource(policy);
            }
            return false;
        }

        private Optional<Boolean> evaluatePolicy(AuthorizationPolicy policy) {
            return evaluateRules(policy.getRules().stream());
        }

        private Optional<Boolean> evaluateRules(Stream<AuthorizationRule> rules) {
            ConcurrentMap<Optional<Boolean>, List<AuthorizationRule>> results = rules.collect(Collectors.groupingByConcurrent(this::evaluateRule));

            // at least one rule returning false overrides any rule returning true
            if (results.containsKey(Optional.of(false))) {
                return Optional.of(false);
            }
            // if there was no rule that returned false, see if there is at least one that returned true
            else if (results.containsKey(Optional.of(true))) {
                return Optional.of(true);
            }

            // if there were no applicable policies or rules, or if none of the rules returned a definitive answer:
            return Optional.empty();
        }

        private Optional<Boolean> evaluateRule(AuthorizationRule rule) {
            Map<String, Object> ruleSubjectAttributes = new HashMap<>();
            ruleSubjectAttributes.putAll(subjectAttributes);
            ruleSubjectAttributes.putAll(getEntities(rule.getSubjectEntitySelectors(), subjectAttributes));

            Map<String, Object> ruleResourceAttributes = new HashMap<>();
            ruleResourceAttributes.putAll(resourceAttributes);
            ruleResourceAttributes.putAll(getEntities(rule.getResourceEntitySelectors(), resourceAttributes));
            return rule.evaluate(ruleSubjectAttributes, ruleResourceAttributes);
        }

        private boolean qualifies(AttributeQualifier qualifier, Map<String, Object> attributes) {
            boolean result;
            Map<String, Object> qualifierAttributes = new HashMap<>(attributes);
            if (qualifier.getEntitySelector() != null) {
                qualifierAttributes.put(qualifier.getEntitySelector().getKey(), getEntity(qualifier.getEntitySelector(), attributes));
            }
            result = qualifier.isQualified(qualifierAttributes);
            return result;
        }

        private Map<String, Object> getEntities(List<EntitySelector> entitySelector, Map<String, Object> attributes) {
            Map<String, Object> attrs = new HashMap<>(attributes);
            Map<String, Object> entityAttributes = new HashMap<>();

            entitySelector.forEach(selector -> {
                Object entity = this.getEntity(selector, attrs);
                attrs.put(selector.getKey(), entity);
                entityAttributes.put(selector.getKey(), entity);
            });
            return entityAttributes;
        }

        private Object getEntity(EntitySelector selector, Map<String, Object> attributes) throws NoSuchElementException {
            try {
                // get entity from cache, or else query all entity providers and
                // store result in the cache
                return selectedEntities.computeIfAbsent(
                    selector,
                    s -> entityProviders.values().stream()
                        .filter(provider -> provider.getEntityType().equals(s.getEntityType()))
                        .filter(provider -> attributes.keySet().containsAll(provider.getRequiredPropertyKeys()))
                        .map(provider -> provider.getEntity(attributes))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .findFirst()
                        .get());
            }
            catch (NoSuchElementException nse) {
                throw new AuthorizationException("No EntityProvider found to satisfy " + selector + " with attributes " + attributes.keySet());
            }
        }
    }

}
