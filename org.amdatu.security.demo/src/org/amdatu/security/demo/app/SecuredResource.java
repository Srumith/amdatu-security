/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.demo.app;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.osgi.service.http.context.ServletContextHelper.REMOTE_USER;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amdatu.security.demo.profile.UserProfile;
import org.amdatu.security.demo.profile.UserProfileService;

@Path("/")
public class SecuredResource {
    // Injected by Felix DM...
    private volatile UserProfileService m_userProfileSrv;

    @GET
    @Path("whoami")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getResource(@Context HttpServletRequest req) {
        String userID = (String) req.getAttribute(REMOTE_USER);

        UserProfile profile = m_userProfileSrv.getUserProfile(userID);
        if (profile == null) {
            return Response.status(NOT_FOUND).build();
        }

        Map<String, String> user = new HashMap<>();
        user.put("email", profile.getEmail());
        user.put("userID", profile.getUserID());
        user.put("name", profile.getEmail().replaceFirst("@.*$", ""));

        return Response.ok(user).build();
    }

}
